package lars.spanningUSA;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Scanner;

import lars.spanningUSA.Edge;
import lars.spanningUSA.Main;
import lars.spanningUSA.Node;
import lars.spanningUSA.WeightedUndirectedGraph;

public class Main {
	Scanner scanner;
	WeightedUndirectedGraph graph;
	ArrayList<String> cities = new ArrayList<String>();
	ArrayList<Edge> edges = new ArrayList<Edge>();
	HashMap<String, HashMap<String,Integer>> distances = new HashMap<String,HashMap<String,Integer>>();
	
	
	public static void main(String[] args) {
		String dataFolder = "data-in\\";
	    String filename = "USA-highway-miles.in";
		
	    long startTime = new Date().getTime();
	    
		Main main = new Main();
		main.loadData(dataFolder + filename);
		main.createGraph();
		main.solve();
		
		long endTime = new Date().getTime();
		
		
		//System.out.println(main.getGraph());
		System.out.println("Time: " + (endTime - startTime) + "ms");
	}
	
	public Main(){
		graph = new WeightedUndirectedGraph();
	}
	
	public void loadData(String filename){
		try {
			scanner = new Scanner(new BufferedInputStream(new FileInputStream(filename)));
		} catch (FileNotFoundException e) {
			System.err.println("Unable to find file at: " + filename);
			e.printStackTrace();
		}
		
		while(scanner.hasNext()){
			String temp = scanner.nextLine();
			if(temp.contains("--")){
				//Add distances
				int i = temp.indexOf("--");
				int bIndex = temp.indexOf("[");
				String from = extractCity(temp.substring(0, i));
				String to = extractCity(temp.substring(i+2, bIndex));
				int distance = Integer.valueOf(temp.substring(bIndex+1, temp.length()-1));
				
				if(distances.containsKey(from))
					distances.get(from).put(to, distance);
				else {
					HashMap<String, Integer> data = new HashMap<String, Integer>();
					data.put(to, distance);
					distances.put(from, data);
				}
				
				//System.out.println(from + "-->" + to + ":" + distance);
			} else {
				//Add cities
				String city = extractCity(temp);
				cities.add(city);
			}
		}
		
	}
	
	private String extractCity(String string){
		//System.out.print(string + "-->");
		String s = string.trim();
		String temp = s.contains("\"") ? s.substring(1,s.length()-1) : s;
		//System.out.println(temp);
		return temp;
	}
	
	public void createGraph(){
		graph = new WeightedUndirectedGraph();
		//Add cities
		for(String c : cities)
			graph.addNode(new Node(c));
	}
	
	public void addAllEdges(){
		for(String city1 : cities){
			HashMap<String, Integer> temp = distances.get(city1);
			if(temp==null)
				continue;
			for(String city2 : cities){
				if(!city1.equals(city2)){
					Integer dist = temp.get(city2);
					if(dist != null)
						graph.addEdge(graph.getNode(city1), graph.getNode(city2), dist);
				}
			}
		}
	}
	
	
	public void solve(){
		ArrayList<Edge> es = new ArrayList<Edge>();
		for(String city1 : cities){
			HashMap<String, Integer> temp = distances.get(city1);
			if(temp==null)
				continue;
			for(String city2 : cities){
				if(!city1.equals(city2)){
					Integer dist = temp.get(city2);
					if(dist != null)
						es.add(new Edge(graph.getNode(city1), graph.getNode(city2), dist));
				}
			}
		}
		
		graph.runPrimsAlgorithm(es);
		
	}
	
	public WeightedUndirectedGraph getGraph(){
		return graph;
	}

}
