package lars.spanningUSA;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.PriorityQueue;

import lars.spanningUSA.Edge;
import lars.spanningUSA.Node;


public class WeightedUndirectedGraph{
	ArrayList<Node> nodes = new ArrayList<Node>();
	ArrayList<Edge> edges = new ArrayList<Edge>();
	
	public WeightedUndirectedGraph(){
	}
	
	
	public void addNode(Node n){
		if(!nodes.contains(n))
			nodes.add(n);
	}
	
	public void addEdge(Node first, Node second, int weight){
		Node f = first;
		Node s = second;
		int fi = nodes.indexOf(first);
		int si = nodes.indexOf(second);
		if(-1 < fi){
			f = nodes.get(fi);
		}
		if(-1 < si){
			s = nodes.get(si);
		}
			
		Edge edge = new Edge(f, s, weight);
		f.addEdge(edge);
		s.addEdge(edge);
		
		edges.add(edge);
		addNode(f);
		addNode(s);
	}
	
	public void addEdge(Edge e){
		addEdge(e.getFrom(), e.getTo(), e.getWeight());
	}
		
	public Node getNode(String name){
		Iterator<Node> i = nodes.iterator();
		while(i.hasNext()){
			Node n = i.next();
				if(n.getName().equals(name))
					return n;
		}
		
		return null;
	}
	
	public void runPrimsAlgorithm(Collection<Edge> collection){
		nodes = new ArrayList<Node>();
		edges = new ArrayList<Edge>();
		Comparator<Edge> comparator = new Comparator<Edge>() {
			@Override
			public int compare(Edge e1, Edge e2) {
				if(e1.getWeight() < e2.getWeight())
					return -1;
				else if(e1.getWeight() == e2.getWeight())
					return 0;
				else 
					return 1;
			}
		};

		//PQ for available edges
		@SuppressWarnings("serial")
		PriorityQueue<Edge> es = new PriorityQueue<Edge>(comparator){

			@Override
			public boolean add(Edge e) {
				//Avoid duplicates!!
				if(contains(e))
					return true;
				return super.add(e);
			}
			
		};
		
		//All edges sorted by corresponding nodes
		HashMap<Node, ArrayList<Edge>> allEdges = new HashMap<Node, ArrayList<Edge>>();
				
		//Add all nodes to nodesLeft - and all the edges to allEdges as well
		HashSet<Node> nodesLeft = new HashSet<Node>();
		for(Edge e : collection){
			nodesLeft.add(e.getFrom());
			nodesLeft.add(e.getTo());
			
			//Add all edges to allEdges
			if(!allEdges.containsKey(e.getFrom()))
				allEdges.put(e.getFrom(), new ArrayList<Edge>());
			if(!allEdges.containsKey(e.getTo()))
				allEdges.put(e.getTo(), new ArrayList<Edge>());
			allEdges.get(e.getFrom()).add(e);
			allEdges.get(e.getTo()).add(e);
		}
		
		Edge edge = collection.toArray(new Edge[0])[0];
		Node root = edge.getFrom();
		nodesLeft.remove(root);
		es.addAll(allEdges.get(root));
		
		//Add the edge with the smallest weight - if it connects to a node that is not a part of the graph yet
		while(!nodesLeft.isEmpty()){
			Edge e = es.poll();
			if(nodesLeft.contains(e.getFrom()) || nodesLeft.contains(e.getTo())){
				addEdge(e);
				nodesLeft.remove(e.getFrom());
				nodesLeft.remove(e.getTo());
				es.addAll(allEdges.get(e.getFrom()));
				es.addAll(allEdges.get(e.getTo()));
			}
		}
		
		
	}
	
	public int countNodes(){
		return nodes.size();
	}
	
	public int countEdges(){
		return edges.size();
	}

	public int totalWeight(){
		int weight = 0;
		for(Edge e : edges)
			weight += (int) e.getWeight();
		return weight;
	}

	//Inspired by Graph from http://algs4.cs.princeton.edu/41undirected/Graph.java.html
	@Override
	public String toString(){
		StringBuilder sb = new StringBuilder();
		String NEWLINE = System.getProperty("line.separator");
		
		for(Edge e : edges)
			sb.append(e.getFrom().getName() + " <--> " + e.getTo().getName() + ": " + e.getWeight() + NEWLINE);
		sb.append("Total nodes: " + countNodes() + "; Total edges: " + countEdges() + "; Total weight: " + totalWeight() + NEWLINE);
		
		return sb.toString();
		
	}
}
	
		