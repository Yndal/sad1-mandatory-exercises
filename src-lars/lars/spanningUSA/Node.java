package lars.spanningUSA;

import java.util.ArrayList;

import lars.spanningUSA.Edge;

public class Node {
	private final String name;
	private final ArrayList<Edge> edges = new ArrayList<Edge>();
	
	
	public Node(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	public void addEdge(Edge edge){
		edges.add(edge);
	}
	
/*	public void addEdgeBetweenThisAnd(Node otherNode, Edge edge){
		
	}

	public boolean hasEdge(Edge edge){
		int i = edges.indexOf(edge);
		return i < 0 ? false : true;
	}
*/
	public ArrayList<Edge> getEdges(){
		return (ArrayList<Edge>) edges;
	}
}