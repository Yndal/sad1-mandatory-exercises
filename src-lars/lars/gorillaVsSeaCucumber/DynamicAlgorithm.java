package lars.gorillaVsSeaCucumber;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import sun.font.CreatedFontTracker;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class DynamicAlgorithm {
	private final String dataFolder = "data-in\\";
	private ArrayList<String> enzymeStrings;
	private ArrayList<String> creatureNames;
	private HashMap<String,HashMap<String, Integer>> scoreboard;

	private int[][] M;

	public static void main(String[] args) {
		int fileToUse = 0;
		String[] filenames = new String[]{"Toy_FASTAs.in","HbB_FASTAs.in"};

		DynamicAlgorithm da = new DynamicAlgorithm();
		if(da.loadData(filenames[fileToUse])){
			//			da.printScoreboard();
			da.solve();
		}
	}





	public void solve(){
		//System.err.println("Solve() has to be implemented");

		M = new int[creatureNames.size()+1][creatureNames.size()+1];
		for(int[] d : M)
			for(int dd : d)
				dd = -1;



		for(int i=0; i<creatureNames.size(); i++)
			for(int a=i+1; a<creatureNames.size(); a++){

//								int i = 0;
//								int a = 1;
				int il = enzymeStrings.get(i).length();
				int al = enzymeStrings.get(a).length();
				//optimal = new char[il < al ? al : il];

				//String val = solve(enzymeStrings.get(i).length()-1, enzymeStrings.get(a).length()-1, enzymeStrings.get(i), enzymeStrings.get(a), 0);
				//				int val = solveLoop(enzymeStrings.get(i).length(), enzymeStrings.get(a).length(), enzymeStrings.get(i), enzymeStrings.get(a));
//				int val = solveLoopString(enzymeStrings.get(i).length(), enzymeStrings.get(a).length(), enzymeStrings.get(i), enzymeStrings.get(a));
				int val = solveRecursively(enzymeStrings.get(i).length()-1, enzymeStrings.get(a).length()-1, enzymeStrings.get(i), enzymeStrings.get(a));
				//int val = solveRecursivelyString(enzymeStrings.get(i).length()-1, enzymeStrings.get(a).length()-1, enzymeStrings.get(i), enzymeStrings.get(a),"","");
				System.out.println(creatureNames.get(i) + "--" + creatureNames.get(a) + ": " + val);

			}
	}


	private int solveLoop(int m, int n, String xx, String yy){
		int stdPenalty = -4;
		int[][] M = new int[m+1][n+1];
		for(int i=0; i<=m; i++)
			M[i][0] = stdPenalty*i;
		for(int j=0; j<=n; j++)
			M[0][j] = stdPenalty*j;

		for(int i=1; i<=m; i++)
			for(int j=1; j<=n; j++){
				int s1 = scoreboard.get(xx.charAt(i-1)+"").get(yy.charAt(j-1)+"") + M[i-1][j-1];
				int s2a = stdPenalty + M[i-1][j];
				int s2b = stdPenalty + M[i][j-1];

				M[i][j] = getHighestInt(s1, s2a, s2b);
			}

		return M[m][n];
	}
	
	private int solveLoopString(int m, int n, String xx, String yy){
		int stdPenalty = -4;

		String[][] sX = new String[m+1][n+1];
		String[][] sY = new String[m+1][n+1];
		for(int a=0; a<=m; a++)
			for(int b=0; b<=n; b++){
				sX[a][b] = "...";
				sY[a][b] = "...";
			}
								
		
		int[][] M = new int[m+1][n+1];
		for(int i=0; i<=m; i++)
			M[i][0] = stdPenalty*i;
		for(int j=0; j<=n; j++)
			M[0][j] = stdPenalty*j;

		for(int i=1; i<=m; i++){
			for(int j=1; j<=n; j++){
				int s1 = scoreboard.get(xx.charAt(i-1)+"").get(yy.charAt(j-1)+"") + M[i-1][j-1];
				int s2a = stdPenalty + M[i-1][j];
				int s2b = stdPenalty + M[i][j-1];

				M[i][j] = getHighestInt(s1, s2a, s2b);
				
				if(M[i][j] == s1){
					sX[i][j] += (xx.charAt(i-1) + "�");
					//System.out.print(xx.charAt(i-1));
					sY[i][j] += (yy.charAt(j-1) + "�");
					//System.out.print(yy.charAt(j-1));
				} else if(M[i][j] == s2a){
					sX[i][j] += "*";// xx.charAt(i-1);
					sY[i][j] += (yy.charAt(j-1) + "�");
				} else if(M[i][j] == s2b){
					sX[i][j] += (xx.charAt(i-1) + "�");
					sY[i][j] += "*";//yy.charAt(j-1);
				}
			//	System.out.println("YyaayY");
			}
			
		}
		
		System.out.println("sX: " + sX[m][n]);
		System.out.println("sY: " + sY[m][n]);
		return M[m][n];
	}


	private int solveRecursively(int m, int n, String xx, String yy){
		int stdPenalty = -4;

		if(m == -1)
			return (n+1)*stdPenalty;
		if(n == -1)
			return (m+1)*stdPenalty;

		int s1 = scoreboard.get(xx.charAt(m)+"").get(yy.charAt(n)+"") + solveRecursively(m-1, n-1, xx, yy);
		int s2a = stdPenalty + solveRecursively(m-1, n, xx, yy);
		int s2b = stdPenalty + solveRecursively(m, n-1, xx, yy);

		return getHighestInt(s1, s2a, s2b);
	}


	private int solveRecursivelyString(int m, int n, String xx, String yy, String optStringXx, String optStringYy){
		int stdPenalty = -4;

		if(m == -1)
			return (n+1)*stdPenalty;
		if(n == -1)
			return (m+1)*stdPenalty;

		int s1 = scoreboard.get(xx.charAt(m)+"").get(yy.charAt(n)+"") + solveRecursivelyString(m-1, n-1, xx, yy, optStringXx, optStringYy);
		int s2a = stdPenalty + solveRecursivelyString(m-1, n, xx, yy, optStringXx, optStringYy);
		int s2b = stdPenalty + solveRecursivelyString(m, n-1, xx, yy, optStringXx, optStringYy);

		int highest = getHighestInt(s1, s2a, s2b);
		if(highest == s2a){
			//optStringXx = optStringXx;
			optStringYy += "-";
		}
		if(highest == s2b){
			optStringXx += "-";
			//optStringYy = optStringYy;
		}
		
		if(m == xx.length()-1 && n == yy.length()-1){
			System.out.println("optXx: " + optStringXx);
			System.out.println("optYy: " + optStringYy);
		}
		return highest;
	}



	private int getHighestInt(int i1, int i2, int i3){
		int res = i1;
		if(res < i2)
			res = i2;
		if(res < i3)
			res = i3;

		return res;
	}



	private void printScoreboard(){
		boolean firstRun = true;
		for(String s : scoreboard.keySet()){
			if(firstRun){
				StringBuilder sb = new StringBuilder();
				sb.append(" ");
				for(String f : scoreboard.keySet()){
					sb.append("  " + f);
				}
				System.out.println(sb.toString());
				firstRun = false;
			}
			System.out.print(s);
			for(String ss : scoreboard.get(s).keySet()){
				System.out.printf(" %2S",scoreboard.get(s).get(ss).toString());
			}
			System.out.println();
		}
	}

	public boolean loadData(String filename){
		final String scoreboardFile = "BLOSUM62.txt";
		enzymeStrings = new ArrayList<String>();
		creatureNames = new ArrayList<String>();

		try (Scanner scannerIn = new Scanner(new BufferedInputStream(new FileInputStream(dataFolder + filename)));
				Scanner scannerScore = new Scanner(new BufferedInputStream(new FileInputStream(dataFolder + scoreboardFile)));){
			String lineIn = scannerIn.nextLine();
			while(scannerIn.hasNext()){
				String name = lineIn.substring(1, lineIn.indexOf(" "));
				String enzymeString = "";
				while(scannerIn.hasNext()){
					lineIn = scannerIn.nextLine();
					if(lineIn.contains(">"))
						break;
					enzymeString += lineIn;
				}

				enzymeStrings.add(enzymeString);
				creatureNames.add(name);

				//				System.out.println(name + ":" + enzymeString);
			}

			//			System.out.println("In-data loaded");


			//Will now load the "scoreboard"
			scoreboard = new HashMap<String,HashMap<String, Integer>>();
			List<String> tempList = new LinkedList<String>();
			String lineScore = "";
			while(scannerScore.hasNext()){
				lineScore = scannerScore.nextLine();
				if(!lineScore.startsWith("#"))
					break;
			}
			lineScore = lineScore.trim();
			int index = lineScore.indexOf(" ");
			int counter = 0;
			while(index != -1){
				String s = lineScore.substring(0, index);
				scoreboard.put(s.trim(), new HashMap<String, Integer>());
				tempList.add(s);

				//Update values to next character
				lineScore = lineScore.substring(index+1).trim();
				index = lineScore.indexOf(" ");
				if(lineScore.length() == 1){
					scoreboard.put(lineScore, new HashMap<String, Integer>());
					tempList.add(lineScore);
				}
			}
			/*	System.out.println("List size: " + tempList.size());
			System.out.println(" ");
			 */while(scannerScore.hasNext()){
				 String sIndex = scannerScore.next();
				 //System.out.print(sIndex);
				 //HashMap<String,Integer> temp = scoreboard.get(sIndex);
				 counter = 0;
				 while(scannerScore.hasNextInt()){
					 int val = scannerScore.nextInt();
					 //	System.out.println(tempList.get(counter) + " +++ " + val + " --> " + counter);
					 scoreboard.get(sIndex).put(tempList.get(counter++), val);
					 //System.out.print(" " + val);

				 }
				 counter++;
				 //System.out.println();
			 }

			 //			System.out.println("Scoreboard loaded");

			 return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}
	}
}
