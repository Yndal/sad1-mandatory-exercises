package lars.behindEnemyLines;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class BehindEnemyLines {
	ArrayList<Node> _nodes;
	ArrayList<DirectedEdge> _edges;
	
	public static void main(String[] args) {
		final String filename = "rail.txt";
		final String dataFolder = "data-in";
		BehindEnemyLines bel = new BehindEnemyLines();
		bel.commence(dataFolder + "\\" + filename);
		
	}
	
	public void commence(String filepath){
		loadData(filepath);
		solve(_nodes,_edges, _nodes.get(0), _nodes.get(_nodes.size()-1));
	}
	
	
	public void loadData(String filepath){
		try(Scanner scanner = new Scanner(new BufferedInputStream(new FileInputStream(filepath)))){
			int nodeAmount = scanner.nextInt();
			_nodes = new ArrayList<Node>(nodeAmount);
			_edges = new ArrayList<DirectedEdge>();
			//System.out.println("nodeAmount: " + nodeAmount);
			
			//String used the rest of the way
			String line = scanner.nextLine();
			
			//Jump to the names of the nodes (they are not unique)
			while(!line.equals("ORIGINS"))
				line = scanner.nextLine();
			//scanner.nextLine(); //TODO
			
			//Load all of the names into the nodes
			int nodeId = 0;
			while(!line.equals("DESTINATIONS")){
				_nodes.add(new Node(nodeId++, line));
			//	System.out.println(line);
				line = scanner.nextLine();
			}
			//Add the destination
			_nodes.add(new Node(nodeId++, line)); //TODO
			
			
			//Load all of the destinations data
			int destAmount = scanner.nextInt();
		//	System.out.println("destAmount: " + destAmount);
			
			while(scanner.hasNext()){
				int startNode = scanner.nextInt();
				int endNode = scanner.nextInt();
				int cap = scanner.nextInt();
			/*	if(cap == -1) 
					cap = Integer.MAX_VALUE;*/
				
				Node ns = _nodes.get(startNode);
				Node ne = _nodes.get(endNode);
				
				DirectedEdge e = new DirectedEdge(ns, ne, cap);
				
				ns.addEdge(e);
				_edges.add(e);
//				System.out.println("start: " + startNode + "; end: " + endNode + "; cap: " + cap);
			}
			
			
			
			
			
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} 
		
	}
	
	
	
	public void solve(List<Node> nodes, List<DirectedEdge> edges, Node source, Node terminal){
		//System.out.println("Source: " + source.getName() + "; Terminal: " + terminal.getName());
		new FordFulkersonGraph(nodes, edges, source, terminal);
	}

}
