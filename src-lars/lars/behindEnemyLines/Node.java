package lars.behindEnemyLines;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Node {
	private final int _id;
	private final String _name;
	private final ArrayList<DirectedEdge> _edges;

	Node(int id, String name){
		_id = id;
		_name = name;
		_edges = new ArrayList<DirectedEdge>();
	}
	
	public int getId(){
		return _id;
	}

	public String getName(){
		return _name;
	}
	
	public void addEdge(DirectedEdge edge){
		_edges.add(edge);
	}
	
	public List<DirectedEdge> getEdges(){
		return new LinkedList<DirectedEdge>(_edges);
	}
}
