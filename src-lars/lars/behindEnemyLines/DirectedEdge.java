package lars.behindEnemyLines;


public class DirectedEdge {
	private final Node _fromNode;
	private final Node _toNode;
	private double _capacity;
	private double _residualCapacity;
	private double _flowValue;

	DirectedEdge(Node from, Node to, int cap){
		_fromNode = from;
		_toNode = to;
		_capacity = cap;
		_residualCapacity = _capacity;
		_flowValue = 0;
	}

	public Node getFromNode(){
		return _fromNode;
	}

	public Node getToNode(){
		return _toNode;
	}

	public void augmentWith(double cap){
		_flowValue += cap;
		_residualCapacity -= cap;
		
	}
	
	public double getCapacity(){
		return _capacity;
	}
	
	public void setCapacity(int newCap){
		_capacity = newCap;
	}
	
	public double getResidualCapacity(){
		return _residualCapacity;
	}
	
	public void setResidualCapacity(int newValue){
		_residualCapacity = newValue;
	}
	
	public double getFlowValue(){
		return _flowValue;
	}
	
	public void setFlowValue(int newValue){
		_flowValue = newValue;
	}
	
}
