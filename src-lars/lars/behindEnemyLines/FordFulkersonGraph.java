package lars.behindEnemyLines;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;


public class FordFulkersonGraph {
	private static final int _infiniteCapacity = -1;
	private ArrayList<Node> _nodes;
	private ArrayList<DirectedEdge> _edges;
	private Node _sourceNode;
	private Node _terminalNode;
	private double _maxFlow;

	public FordFulkersonGraph(List<Node> nodes, List<DirectedEdge> edges, Node sourceNode, Node terminalNode){
		setUpGraph(nodes, edges, sourceNode, terminalNode);

		Queue<DirectedEdge> newPath = dijkstra();//BFS(_sourceNode, _terminalNode);

		while(newPath != null){
			double cap = lowestAvailableCapacity(newPath);
			augment(newPath, cap);

			System.out.println("cap: " + cap + " - maxFlow: " + _maxFlow);


			newPath = dijkstra();// BFS(_sourceNode, _terminalNode);

		}

		//TODO Find bottleneck

	}

	private void setUpGraph(List<Node> nodes, List<DirectedEdge> edges, Node source, Node terminal) {
		_nodes = new ArrayList<Node>();
		_edges = new ArrayList<DirectedEdge>(edges.size());
		_sourceNode = source;
		_terminalNode = terminal;
		_maxFlow = 0;

		for(Node n : nodes)
			_nodes.add(n);
		for(DirectedEdge e : edges)
			_edges.add(e.getFromNode().getId(), e);

	}

	static int counter = 0;
	static int precounter = 0;
	private Queue<DirectedEdge> BFS(Node source, Node terminal){
		precounter++;
		System.out.println("Precounter: " + precounter);
		int[] path = new int[_edges.size()];

		Queue<Node> queue = new LinkedList<Node>();
		Set<Node> prevNodes = new HashSet<Node>();
		queue.add(source);
		prevNodes.add(source);

		while(!queue.isEmpty()){
			Node curNode = queue.poll();
			if(curNode == terminal){
				break;//return path;
			}
			for(DirectedEdge edge : curNode.getEdges()){
				if((0 < edge.getResidualCapacity() || edge.getResidualCapacity() == _infiniteCapacity) && 
						!prevNodes.contains(edge.getToNode())){
					path[edge.getToNode().getId()] = curNode.getId();
					prevNodes.add(edge.getToNode());
					queue.add(edge.getToNode());	
				}
			}
		}

		/*
		 * Create structure to return
		 */
		Stack<Integer> pathStack = new Stack<Integer>();

		//Reverse the reversed path ;o)
		int i = terminal.getId();
		//pathStack.push(i);
		while(i != _sourceNode.getId()){
			pathStack.push(i);
			i = path[i];
		}
		pathStack.push(_sourceNode.getId());
		if(pathStack.size() <= 2)
			return null; //Only Source and Terminal was added - not a valid path!

		//Create the structure
		Queue<DirectedEdge> resultPath = new LinkedList<DirectedEdge>();

		int fromI = pathStack.pop();
		int toI = pathStack.pop();
		Node from;
		Node to;
		boolean notDone = false;
		while(!notDone){
			if(pathStack.isEmpty())
				notDone = true;

			from = _nodes.get(fromI);
			to = _nodes.get(toI);

			DirectedEdge edge = null;
			for(DirectedEdge e : from.getEdges())
				if(e.getToNode().equals(to)){
					edge = e;
					break;
				}

			resultPath.add(edge);
			System.out.println(edge.getFromNode().getName() + "(" + edge.getFromNode().getId() + ")" + " --> " + edge.getToNode().getName() + "(" + edge.getToNode().getId() + ") - Res-cap: " + edge.getResidualCapacity());

			if(!notDone){
				fromI = toI;
				toI = pathStack.pop();
			}
		}

		printPath(resultPath.iterator());
		System.out.println("Counter: " + ++counter);
		return resultPath;
	}

	private Queue<DirectedEdge> dijkstra(){
		final double[] dist = new double[_nodes.size()];
		int[] prev = new int[_nodes.size()];
		Queue<Node> q = new PriorityQueue<>(new Comparator<Node>() {

			@Override
			public int compare(Node n1, Node n2) {
				if(dist[n1.getId()] < dist[n2.getId()])
					return -1;
				else if (dist[n1.getId()] < dist[n2.getId()])
					return 0;
				else
					return 1;
			}
		});
		
		
		dist[_sourceNode.getId()] = 0;
		for(Node node : _nodes){
			if(!node.equals(_sourceNode)){
				dist[node.getId()] = Double.POSITIVE_INFINITY;
				prev[node.getId()] = -1;
			}
			q.add(node);
		}

		while(!q.isEmpty()){
			Node node = q.remove(); 
			
			for(DirectedEdge edge : node.getEdges()){
				double alt = dist[node.getId()] + 1;//edge.getResidualCapacity();
				if(alt < dist[edge.getToNode().getId()]){
					dist[edge.getToNode().getId()] = alt;
					prev[edge.getToNode().getId()] = node.getId();
				}
			}
		}
		
		Queue<DirectedEdge> result = new LinkedList<>();
		
		int u = _terminalNode.getId();
		while(prev[u] != _sourceNode.getId()){
			DirectedEdge edge = null;
			for(DirectedEdge e : _nodes.get(prev[u]).getEdges())
				if(e.getToNode().equals(_nodes.get(u))){
					edge = e;
					break;
				}

			result.add(edge);
			u = prev[u];
		}
		
		
		
		return result;
	}
	
	
	private double lowestAvailableCapacity(Queue<DirectedEdge> edges){
		double res = Double.POSITIVE_INFINITY;
		for(DirectedEdge e : edges)
			if(e.getResidualCapacity() < res &&
					e.getResidualCapacity() != 0 &&
					e.getResidualCapacity() != _infiniteCapacity)
				res = e.getResidualCapacity();

		return res;
	}


	private void augment(Queue<DirectedEdge> path, double cap){
		for(DirectedEdge edge : path){
			edge.augmentWith(cap);
		}
		_maxFlow += cap;
	}


	public double getMaxFlow(){
		return _maxFlow;
	}


	//For debugging
	private void printPath(Iterator<DirectedEdge> path){
		while(path.hasNext()){
			DirectedEdge edge = path.next();
			Node from = edge.getFromNode();
			Node to = edge.getToNode();
			System.out.println(from.getId() + ": " + from.getName() + " --> " + to.getId() + ": " + to.getName() + " \tCap: " + edge.getCapacity());
		}
	}

	/*private void printPath(int[] path, int target){
		int i = target;
		int prevI = path[i];
		Node curNode = _nodes.get(i);
		Node prevNode = _nodes.get(prevI);
		String s = "";
		while(i != _sourceNode.getId()){
			s = prevNode.getName() + "(" + prevNode.getId() + ")" + " --> " + curNode.getName() + "(" + curNode.getId() + ")" + "\n" + s;

			i = prevI;
			prevI = path[i];
			curNode = _nodes.get(i);
			prevNode = _nodes.get(prevI);
		}

		System.out.println(s);
	}*/

}