package lars.closestPair;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;


public class ClosestPair {
	Scanner scanner;
	ArrayList<String> ids;
	HashMap<String,Point> coords;

	Comparator<Point> xComp = new Comparator<Point>() {
		@Override
		public int compare(Point p1, Point p2) {
			if(p1.x < p2.x)
				return -1;
			if(p1.x == p2.x)
				return 0;
			else
				return 1;
		}
	};
	Comparator<Point> yComp = new Comparator<Point>() {
		@Override
		public int compare(Point p1, Point p2) {
			if(p1.y < p2.y)
				return -1;
			if(p1.y == p2.y)
				return 0;
			else
				return 1;
		}
	};


	public static void main(String[] args) {
		String dataFolder = "data-in\\";
		File folder = new File(dataFolder);
		File[] files = folder.listFiles();
		//files = new File[]{new File("close-pairs-2.in")};

		long start = new Date().getTime();
		ClosestPair cp = new ClosestPair();

		for(File f : files){
			if(f.getName().endsWith(".tsp")){
			//String filename = "bier127.tsp";//*/"close-pairs-6.in";

			List<Point> points = cp.loadData(dataFolder + f.getName());

			List<Point> pointsBruteForce = cp.solveBruteForce(points);
			//cp.solveDivideAndConquer(points);
			
			System.out.println(dataFolder + f.getName() + " " + points.size() + " " + calcDist(pointsBruteForce.get(0),pointsBruteForce.get(1)));
			}
		}
		long end = new Date().getTime();
		System.out.println("Total time: " + (end-start) + "ms");

	}

	public ClosestPair(){
		ids = new ArrayList<String>();
		coords = new HashMap<String,Point>();
	}


	public List<Point> loadData(String filename){
		try {
			scanner = new Scanner(new BufferedInputStream(new FileInputStream(filename)));
		} catch (FileNotFoundException e) {
			System.err.println("Unable to find file at: " + filename);
			e.printStackTrace();
		}

		//File is found
		//System.out.println("File: " + filename);

		//Skip the first  lines if needed
		String s;
		if(filename.endsWith(".tsp")){
			while(scanner.hasNext()){
				s = scanner.nextLine();
				if(s.trim().equals("NODE_COORD_SECTION"))
					break;
			}
		}

		ArrayList<Point> res = new ArrayList<Point>();

		//Create data structure
		while(scanner.hasNext()){
			s = scanner.nextLine();
			if(s.trim().equals("EOF")) //Will be the end of the file
				break;

			s = s.trim();
			int index = s.indexOf(" ");
			String id = s.substring(0, index);
			s = s.substring(index+1).trim();
			
			index = s.indexOf(" ");
			double x = Double.valueOf(s.substring(0,index));//s scanner.nextInt();
			
			s = s.substring(index+1).trim();
			double y = Double.valueOf(s);

			Point point = new Point(x,y);

			ids.add(id);
			res.add(point);
			coords.put(id, point);
		}

		return res;
	}

	public Collection<Point> solveDivideAndConquer(List<Point> points){
		if(points.size() == 2) return new ArrayList<Point>(points);
		if(points.size() < 2) return null;

		Queue<Point> xSortedPoints = new PriorityQueue<Point>(xComp);
		Queue<Point> ySortedPoints = new PriorityQueue<Point>(yComp);

		for(Point p : points){
			xSortedPoints.add(p);
			ySortedPoints.add(p);
		}

		return closestPairRec(xSortedPoints, ySortedPoints);
	}

	private Queue<Point> closestPairRec(Queue<Point> xSortedPoints, Queue<Point> ySortedPoints){
		if(xSortedPoints.size() <= 3)
			return new LinkedList<Point>(solveBruteForce(new LinkedList<Point>(xSortedPoints)));

		Queue<Point> qx = new PriorityQueue<Point>(xComp);
		Queue<Point> qy = new PriorityQueue<Point>(yComp);
		Queue<Point> rx = new PriorityQueue<Point>(xComp);
		Queue<Point> ry = new PriorityQueue<Point>(yComp);

		int size = xSortedPoints.size();
		for(int i=0; i<size; i++){
			if(i < size/2){
				qx.add(xSortedPoints.remove());
				qy.add(ySortedPoints.remove());
			} else {
				rx.add(xSortedPoints.remove());
				ry.add(ySortedPoints.remove());
			}
		}

		Queue<Point> closestQ = closestPairRec(qx, qy);
		Queue<Point> closestR = closestPairRec(rx, ry);

		Point q1 = closestQ.poll();
		Point q2 = closestQ.poll();
		Point r1 = closestR.poll();
		Point r2 = closestR.poll();
		double distQ = calcDist(q1, q2);
		double distR = calcDist(r1, r2);


		Queue<Point> closestPair;
		double minDist;
		if(distQ < distR){
			minDist = distQ;
			closestPair = new LinkedList<Point>();
			closestPair.add(q1);
			closestPair.add(q2);
		} else {
			minDist = distR;
			closestPair = new LinkedList<Point>();
			closestPair.add(r1);
			closestPair.add(r2);
		}

		double middLineX = getHighestX(xSortedPoints);
		System.out.println("X: " + middLineX);
		return closestPair;
	}

	private double getHighestX(Queue<Point> points){
		double x = Double.NEGATIVE_INFINITY;
		Iterator<Point> ip = points.iterator();
		while(ip.hasNext()){
			Point p = ip.next();
			if(x < p.x)
				x = p.x;
		}
		return x;
	}

	public List<Point> solveBruteForce(List<Point> points){
		if(points.size() == 2) return new ArrayList<Point>(points);
		if(points.size() < 2){
			System.err.println("NOOOOOOO!!!");
			return null;
		}

		Point p1 = null;
		Point p2 = null;
		double closestDist = Double.POSITIVE_INFINITY;

		for(int a=0; a<points.size(); a++)
			for(int b=a+1; b<points.size(); b++){
				double dist = calcDist(points.get(a), points.get(b));
				if(dist < closestDist){
					closestDist = dist;
					p1 = points.get(a);
					p2 = points.get(b);
				}
			}

		ArrayList<Point> result = new ArrayList<Point>();
		result.add(p1);
		result.add(p2);

		if(p1==null || p2==null)
			System.err.println("THIS WILL FAIL!!!");

		return result;
	}

	private static double calcDist(Point p1, Point p2){
		double xDist = p2.x - p1.x;
		double yDist = p2.y - p1.y;
		return Math.sqrt(xDist*xDist + yDist*yDist);
	}

}
