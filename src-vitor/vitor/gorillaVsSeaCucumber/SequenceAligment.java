package vitor.gorillaVsSeaCucumber;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class SequenceAligment {

	private ArrayList<Species> listOfSpecies = new ArrayList<Species>();
	private int[][] Blosum;
	private char[] BlosumIndex;

	public static void main(String[] args) throws FileNotFoundException {

		long startTime = System.nanoTime();

		SequenceAligment bfs = new SequenceAligment(args);

		long endTime = System.nanoTime();
		long duration = (endTime - startTime);

		System.out.println("Time spent in seconds: " + (double) duration
				/ (double) 1000000000);

	}

	public SequenceAligment(String[] args) throws FileNotFoundException {

		GetMatrixFromFile(args[0]);

		// read file
		GetDataFromFile(args[1]);

		// run algorithm
		SequenceAligmentAlgorithm();

	}

	private void SequenceAligmentAlgorithm() {

		for (Species specie1 : listOfSpecies) {
			for (Species specie2 : listOfSpecies) {
				if (!specie1.equals(specie2) && !specie2.WasCompared(specie1)) {

					String protein1 = specie1.getProtein();
					String protein2 = specie2.getProtein();

					specie1.InitializeMemory(protein1.length() + 1, 
							protein2.length() + 1);

					int result = LevenshteinDistance(protein1,
							protein1.length()-1, protein2, protein2.length()-1,
							specie1.getMemory());
					
					//int result = compute(protein1,protein2, protein1.length(), protein2.length());

					int sdf = result;

					specie1.setComparedWith(specie2);
					specie1.setResult(null, result);
				}

			}
		}

	}
	
	private int LevenshteinDistance(String protein1, int len1, String protein2,
			int len2, int[][] M) {

		int cost = 0;
		if (len1 == 0)
			//return len2;
			return M[0][len2];
		if (len2 == 0)
			//return len1;
			return M[len1][0];

		char letter1 = protein1.charAt(len1);
		char letter2 = protein2.charAt(len2);

		int index1 = new String(BlosumIndex).indexOf(letter1);
		int index2 = new String(BlosumIndex).indexOf(letter2);

		if(M[len1][len2] == 0){
				cost = Blosum[index1][index2];
		}
		
		
		int s1 = LevenshteinDistance(protein1, len1 - 1, protein2, len2, M) + Blosum[index1][Blosum.length-1];
		int s2 = LevenshteinDistance(protein1, len1, protein2, len2 - 1, M) + Blosum[index1][Blosum.length-1];
		int s3 = LevenshteinDistance(protein1, len1 - 1, protein2, len2 - 1, M) + cost;

		int sx = Math.max(s1, s2);
		int res = Math.max(sx, s3);

		M[len1][len2] = res;		
		return res;
		
	}
	

	private void GetMatrixFromFile(String fileName)
			throws FileNotFoundException {

		File source = new File(fileName);
		Scanner scan = new Scanner(source);

		String available = scan.nextLine();

		while (available.startsWith("#")) {
			available = scan.nextLine();
		}

		String matrixColumns = available;
		String[] col = matrixColumns.split(" ");
		String[] columns = RemoveSpaceFromArray(col);

		int i = 0;
		Blosum = new int[columns.length][columns.length];
		BlosumIndex = new char[columns.length];
		for (String value : columns) {
			BlosumIndex[i] = value.charAt(0);
			;
			i++;
		}

		i = 0;
		while (scan.hasNextLine()) {
			String matrixRows = scan.nextLine();
			String[] row = matrixRows.split(" ");
			String[] rowValues = RemoveSpaceFromArray(row);

			for (int j = 1; j < rowValues.length; j++) {
				Blosum[i][j - 1] = Integer.parseInt(rowValues[j]);
			}
			i++;
		}
	}

	private void GetDataFromFile(String fileName) throws FileNotFoundException {

		File source = new File(fileName);
		Scanner scan = new Scanner(source);

		String available = null;

		while (scan.hasNextLine()) {

			if (available == null)
				available = scan.nextLine();

			Species newSpacies = new Species();

			if (available.startsWith(">")) {
				String[] spliName = available.split(" ");
				String name = spliName[0].substring(1);
				newSpacies.setName(name);
			}

			String proteins = "";
			available = scan.nextLine();
			while (!available.startsWith(">")) {
				proteins += (available);
				if (scan.hasNext())
					available = scan.nextLine();
				else
					break;
			}
			newSpacies.setProtein(proteins);

			listOfSpecies.add(newSpacies);
		}

	}

	private String[] RemoveSpaceFromArray(String[] pointInfo) {

		ArrayList<String> list = new ArrayList<String>();

		for (String s : pointInfo) {
			if (s != null && s.length() > 0) {
				list.add(s);
			}
		}

		pointInfo = list.toArray(new String[list.size()]);

		return pointInfo;

	}

}
