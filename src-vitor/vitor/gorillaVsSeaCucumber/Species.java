package vitor.gorillaVsSeaCucumber;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Species {

	private String Name;
	private String Protein;
	private ArrayList<Species> ComparedWith = new ArrayList();
	Map<String, Integer> Compared = new HashMap<String, Integer>();
	
	int[][] M;

	public String getName() {
		return Name;
	}

	public String getProtein() {
		return Protein;
	}

	public void setProtein(String prot) {
		Protein = prot;
	}

	public void setName(String n) {
		Name = n;
	}

	public boolean WasCompared(Species specie) {
		return ComparedWith.contains(specie);
	}

	public void setComparedWith(Species specie) {
		ComparedWith.add(specie);
	}

	public void setResult(String comparison, int value) {
		Compared.put(comparison, value);
	}
	
	public void InitializeMemory(int size1, int size2) {
		M = new int[size1][size2];
	}
	
	public int[][] getMemory() {
		return M;
	}

}
