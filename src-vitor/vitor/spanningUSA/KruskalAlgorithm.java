package vitor.spanningUSA;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class KruskalAlgorithm {
	
	private ArrayList<Node> nodeList = new ArrayList();
	private ArrayList<ArrayList<Node>> tree = new ArrayList();
	private ArrayList<Edge> edgeList = new ArrayList();

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		
		 long startTime = System.nanoTime();
		
		File file = new File(args[0]);
		Scanner cityList = new Scanner(file);
		KruskalAlgorithm bfs = new KruskalAlgorithm(cityList);
		
		long endTime = System.nanoTime();
	    long duration = (endTime - startTime);
	    System.out.println("Time spent in seconds: " + (double)duration/(double)1000000000);
	}

	public KruskalAlgorithm(Scanner scan) {
		GetDataFromFile(scan);
		OrderEdgesByDistance();
		Kruskal();
		
		System.out.println("Total Amount: " + totalAmount);
	}
	
	int totalAmount;
	private void Kruskal() {
		
		totalAmount = 0;
		
		for (Edge edgeItem : edgeList) {
			
			int nodeFromInTree = CheckIfNodeExist(edgeItem.getFrom());
			int nodeToInTree = CheckIfNodeExist(edgeItem.getTo());
			
			if (nodeFromInTree == -1 && nodeToInTree == -1 ){
				ArrayList<Node> newlistnew = new ArrayList();
				newlistnew.add(edgeItem.getFrom());
				newlistnew.add(edgeItem.getTo());
				tree.add(newlistnew);
			}
			else if  (nodeToInTree== -1 && nodeFromInTree != -1){
				ArrayList<Node> list = tree.get(nodeFromInTree);
				list.add(edgeItem.getTo());
			}
			else if (nodeToInTree != -1 && nodeFromInTree == -1) {
				ArrayList<Node> list = tree.get(nodeToInTree);
				list.add(edgeItem.getFrom());
			}
			else if (nodeToInTree != -1 && nodeFromInTree != -1){
				
				if(nodeFromInTree  == nodeToInTree) continue;
				
				ArrayList<Node> listFrom = tree.get(nodeFromInTree);
				ArrayList<Node> listTo = tree.get(nodeToInTree);
				
				// union
				for (Node node : listTo) {
					listFrom.add(node);
				}
				
				tree.remove(nodeToInTree);
				
			}
			
			totalAmount += edgeItem.getValue();
				
		}
		
	}

	private int CheckIfNodeExist(Node node) {
		
		int countPosition= 0;
		for (ArrayList<Node> list : tree) {
			
			if(list.contains(node)) return countPosition;
			
			countPosition++;
		}
		
		return -1;
	}

	private void OrderEdgesByDistance() {
		Collections.sort(edgeList);
	}
	
	private void GetDataFromFile(Scanner scan) {
		String available;

		boolean name = false;
		while (!name) {
			available = scan.nextLine();

			if (!available.endsWith("]")) {
				String nodeName;
				if (available.startsWith("\"")) {
					available = available.replaceAll("\\s+","");
					nodeName = available.substring(1, available.length() - 1);

				} else {
					available = available.replaceAll("\\s+","");
					nodeName = available;
				}
				Node newNode = new Node();
				newNode.name = nodeName;
				nodeList.add(newNode);
			} else {
				name = true;
			}
		}

		while (scan.hasNextLine()) {

			available = scan.nextLine();

			int lastSpace = available.lastIndexOf(" ");	
			String edge = available.substring(lastSpace+1).trim();
			edge = edge.replaceAll("\\s+","");
			String distance = edge.substring(1, edge.length() - 1);
			int edegeValue = Integer.parseInt(distance);

			
			String[] nodesLine = available.substring(0,lastSpace).split("--");
			String nodeFromName = nodesLine[0];
			String nodeToName = nodesLine[1];
			
			nodeFromName = nodeFromName.replaceAll("\\s+","");
			nodeToName = nodeToName.replaceAll("\\s+","");
			if (nodeFromName.startsWith("\"")) nodeFromName =nodeFromName.substring(1, nodeFromName.length() - 1);
			if (nodeToName.startsWith("\"")) nodeToName =nodeToName.substring(1, nodeToName.length() - 1);

			Node nodeFrom = null;
			Node nodeTo = null;
			for (Node item : nodeList) {
				if (item.name.equals(nodeFromName))
					nodeFrom = item;
			}
			for (Node item : nodeList) {
				if (item.name.equals(nodeToName))
					nodeTo = item;
			}

			Edge newEdge = new Edge();
			newEdge.setValue(edegeValue);
			newEdge.setFrom(nodeFrom);
			newEdge.setTo(nodeTo);
			edgeList.add(newEdge);

		}
	}	
}
