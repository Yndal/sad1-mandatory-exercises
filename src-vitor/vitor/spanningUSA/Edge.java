package vitor.spanningUSA;

public class Edge implements Comparable<Edge> {
	private int value;
	private Node From;
	private Node To;

	public int getValue() {
		return value;
	}

	public void setValue(int val) {
		value = val;
	}

	public Node getFrom() {
		return From;
	}

	public void setFrom(Node node) {
		From = node;
	}

	public Node getTo() {
		return To;
	}

	public void setTo(Node node) {
		To = node;
	}

	public int compareTo(Edge other) {
		//return Integer.toString(value).compareTo(Integer.toString(other.value));
		return value - other.value;
	}
}
