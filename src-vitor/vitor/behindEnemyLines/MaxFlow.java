package vitor.behindEnemyLines;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import javax.sound.sampled.ReverbType;
import javax.xml.crypto.NodeSetData;

public class MaxFlow {

	public static void main(String[] args) throws FileNotFoundException {
		File file = new File(args[0]);

		MaxFlow sdf = new MaxFlow(file);
	}

	private ArrayList<Node> nodeList = new ArrayList();
	private ArrayList<Edge> edgeList = new ArrayList();
	private ArrayList<Edge> originalEdgeList = new ArrayList();

	private int count = 0;

	private ArrayList<String> nodes = new ArrayList<>();

	private MaxFlow(File file) throws FileNotFoundException {

		Scanner nodesFile = new Scanner(file);

		ReadFile(nodesFile);

		CopyList();

		MaxFlowAlgorithm();
	}

	private void CopyList() {
		for (Edge edge : edgeList) {
			originalEdgeList.add(edge);
		}

	}

	private void MaxFlowAlgorithm() throws FileNotFoundException {

		boolean existPath = true;
		int totalFlow = 0;

		while (existPath) {

			Graph graph = new Graph(nodeList, edgeList);
			DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);

			dijkstra.execute(nodeList.get(0));
			LinkedList<Node> path = dijkstra.getPath(nodeList.get(nodeList
					.size() - 1));

			if (path == null)
				break;

			ArrayList<Edge> edgePath = getEdgePath(path);

			// update edges
			UpdateEdges(edgePath);

		}

		System.out.println("Max flow : " + count);

		System.out.println("Min-cut arcs: ");
		int minCapacity = 0;
		List<Edge> list = minCut();
		for (Edge e : list) {
			System.out.println("\tEdge (id: " + e + ") " + "weight: "
					+ e.getWeight());
			minCapacity += e.getWeight();
		}

		System.out.println("Min-cut capacity: " + minCapacity);

	}

	public List<Edge> minCut() throws FileNotFoundException {

		Graph graph = new Graph(nodeList, edgeList);
		DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);

		dijkstra.execute(nodeList.get(0));

		Set<Integer> left_set = new HashSet<>();
		Set<Integer> right_set = new HashSet<>();

		for (int i = 1; i < nodeList.size() - 1; i++) {
			if (dijkstra.getPath(nodeList.get(i)) != null)
				left_set.add(i);
			else
				right_set.add(i);
		}

		List<Edge> minCutEdges = new ArrayList<>();

		for (int to : left_set) {
			for (int from : right_set) {
				for (Edge edge : originalEdgeList) {
					int sourceName = Integer.parseInt(edge.getSource()
							.getName());
					int destinationName = Integer.parseInt(edge
							.getDestination().getName());
					if ((sourceName == from && destinationName == to)) {
						if (!minCutEdges.contains(edge))
							minCutEdges.add(edge);
					}
				}

			}

		}

		return minCutEdges;
	}

	private ArrayList<Edge> getEdgePath(LinkedList<Node> path) {

		ArrayList<Edge> edges = new ArrayList<Edge>();

		for (int i = 0; i < path.size() - 1; i++) {

			String from = path.get(i).getName();
			String to = path.get(i + 1).getName();

			for (Edge edge : edgeList) {
				if (edge.getSource().getName().equals(from)
						&& edge.getDestination().getName().equals(to)) {
					edges.add(edge);
					break;
				}
			}
		}

		return edges;
	}

	private void UpdateEdges(ArrayList<Edge> edgePath) {

		int minEdge = Integer.MAX_VALUE;

		for (Edge edge : edgePath) {
			if (edge.getWeight() != -1) {
				if (minEdge > edge.getWeight())
					minEdge = edge.getWeight();
			}
		}

		count += minEdge;

		for (Edge ePath : edgePath) {

			int index = edgeList.indexOf(ePath);
			Edge edge = edgeList.get(index);

			Edge reverseEdge = null;
			for (Edge ed : edgeList) {
				if (ed.getSource().getName()
						.equals(edge.getDestination().getName())
						&& ed.getDestination().getName()
								.equals(edge.getSource().getName())) {
					reverseEdge = ed;
					break;
				}
			}

			if (edge.getWeight() == -1)
				continue;

			Edge newEdge = null;
			if (reverseEdge == null) {
				newEdge = new Edge(edge.getDestination(), edge.getSource(),
						edge.getRealWeight());
				edge.setWeight(edge.getWeight() - minEdge);
				newEdge.setWeight(edge.getRealWeight() - edge.getWeight());
				newEdge.setReverse();
				newEdge.setFirstTime(false);
				edgeList.add(newEdge);
			} else {
				if (reverseEdge.getFirstTime()) { // undirective
					edge.setWeight(edge.getWeight() - minEdge);
					reverseEdge.setWeight(edge.getRealWeight()
							- edge.getWeight());
					reverseEdge.setFirstTime(false);
				} else {
					edge.setWeight(edge.getWeight() - minEdge);
					reverseEdge.setWeight(edge.getRealWeight()
							- edge.getWeight());
				}
			}

			if (edge.getWeight() <= 0) {
				edgeList.remove(edge);
			}

		}
	}

	private void ReadFile(Scanner nodesFile) {

		String available;

		int numberOfNode = Integer.parseInt(nodesFile.nextLine());

		available = nodesFile.nextLine();
		int i = 0;
		while (!available.startsWith("119")) {

			Node node = new Node("node" + i, String.valueOf(i));
			// node.setName();
			nodeList.add(node);

			i++;
			available = nodesFile.nextLine();
		}

		while (nodesFile.hasNextLine()) {
			available = nodesFile.nextLine();

			String[] arcs = available.split(" ");

			Node nodeFrom = nodeList.get(Integer.parseInt(arcs[0]));
			Node nodeTo = nodeList.get(Integer.parseInt(arcs[1]));
			int capacity = Integer.parseInt(arcs[2]);

			Edge edge = new Edge(nodeFrom, nodeTo, capacity);
			edgeList.add(edge);

			if (capacity != -1) {
				Edge edgeReverse = new Edge(nodeTo, nodeFrom, capacity);
				edgeReverse.setFirstTime(true);
				edgeReverse.setReverse();
				edgeList.add(edgeReverse);
			}

		}
	}

}
