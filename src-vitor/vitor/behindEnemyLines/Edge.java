package vitor.behindEnemyLines;


public class Edge  implements Comparable<Edge> {
  //private final String id; 
  private final Node source;
  private final Node destination;
  private int weight;
  private final int realWeight;
  
  private boolean isFirstTime;
  private boolean isReverseNode;
  
  public Edge(Node source, Node destination, int weight) {
    //this.id = id;
    this.source = source;
    this.destination = destination;
    this.weight = weight;
    this.realWeight = weight;
  }
 
  /*
  public String getId() {
    return id;
  }
  */
  
  public int getRealWeight() {
	return realWeight;
  }
  
  public Node getDestination() {
    return destination;
  }

  public Node getSource() {
    return source;
  }
  public int getWeight() {
    return weight;
  }
  public void setWeight(int w) {
	    weight=w;
	  }
  
  @Override
  public String toString() {
    return source + " " + destination;
  }
  
  public int compareTo(Edge other) {
		//return Integer.toString(value).compareTo(Integer.toString(other.value));
		return Integer.parseInt(source.getName()) - Integer.parseInt(other.source.getName());
	}

	public boolean getFirstTime() {
		return isFirstTime;
	}
	
	public void setFirstTime(boolean val) {
		isFirstTime = val;
	}

	public void setReverse() {
		isReverseNode = true;
	}
	
	public boolean getReverse() {
		return isReverseNode;
	}
  
} 