package vitor.closestPair;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class ClosePoints {

	private ArrayList<Point> pointList = new ArrayList<Point>();
	private ArrayList<Point> sortedPointList = new ArrayList<Point>();
	private ArrayList<Point> pointInRange = new ArrayList<>();
	private ArrayList<Point> sortedPointInRange = new ArrayList<>();

	private double distance;
	private String nameFrom;
	private String nameTo;
	private double delta;
	private double limit;
	private double halfDelta;
	private int numberOfNodes;

	public static void main(String[] args) throws FileNotFoundException {

		long startTime = System.nanoTime();

		ClosePoints bfs = new ClosePoints();

		long endTime = System.nanoTime();
		long duration = (endTime - startTime);

		System.out.println("Time spent in seconds: " + (double) duration
				/ (double) 1000000000);
	}

	public ClosePoints() throws FileNotFoundException{

		final File folder = new File("data-in");

		for (final File fileEntry : folder.listFiles()) {

			// read different sort of files			
			
			Scanner pointsList = new Scanner(fileEntry);
			if (fileEntry.getName().endsWith("tsp")){
				CleanVariables();
				GetDataFromTSPFile(pointsList);
				ClosePointsAlgorithm();
				
				System.out.println(fileEntry.getName() + " " + numberOfNodes + " " + distance);
			}
			//System.out.println(nameFrom + "-" + nameTo);
		}
	}

	private void CleanVariables() {
		pointList = new ArrayList<Point>();
		sortedPointList = new ArrayList<Point>();
		pointInRange = new ArrayList<>();
		sortedPointInRange = new ArrayList<>();

		distance = 0;
		nameFrom  = "";
		nameTo = "";
		delta = 0;
		limit = 0;
		halfDelta = 0;
	}

	public void ClosePointsAlgorithm(){

		// order by x
		OrderPointsByX();

		// calculate L, middle of points
		numberOfNodes = sortedPointList.size();
		Point rightLast = sortedPointList.get(sortedPointList.size() - 1);
		Point rightFirst = sortedPointList.get(sortedPointList.size() / 2);
		Point leftLast = sortedPointList.get((pointList.size() / 2) - 1);
		limit = leftLast.getX() + ((rightFirst.getX() - leftLast.getX()) / 2);
		rightLast.setLast();
		leftLast.setLast();

		int lastLeftIndex = sortedPointList.indexOf(leftLast);

		// get shortest point distances from both sides
		double shortestDistance = getShortestDistanceFromBothSides(lastLeftIndex);

		distance = shortestDistance;
		delta = shortestDistance;
		halfDelta = shortestDistance / 2;

		// get the points, from L to delta, in both sides
		getPointsInRange(leftLast);

		// order by Y
		OrderPointsByY();

		// set point in a box
		SetEachPointInBox();

		// calculate if there is a better distance
		CalculateDistancesBetweenAxisYPoints();

	}

	private void CalculateDistancesBetweenAxisYPoints() {

		for (int i = 0; i <= sortedPointInRange.size() - 1; i++) {
			Point point = sortedPointInRange.get(i);
			// Point nextPoint = sortedPointInRange.get(i + 1);

			int[] pointBox = point.getBox();

			/*
			 * for (int j = 0; j <= sortedPointInRange.size() - 1; j++) { if
			 * (!point.equals(sortedPointInRange.get(j))) { double d =
			 * sortedPointInRange.get(j).distance(point); if (d < distance)
			 * distance = d; } }
			 */

			for (int j = i + 1; j < sortedPointInRange.size() - 1; j++) {
				if (sortedPointInRange.size() - 1 >= j) {
					Point p = sortedPointInRange.get(j);
					int[] box = p.getBox();
					int xBoxDistance = Math.abs(pointBox[0] - box[0]);
					int yBoyDistance = Math.abs(pointBox[1] - box[1]);
					if (yBoyDistance <= 0 && xBoxDistance <= 2) { 
						double d = point.distance(p);
						if (d < distance)
							distance = d;
						continue;
					} else
						break;
				}
				break;
			}

			for (int j = i - 1; j >= 0; j--) {
				Point p = sortedPointInRange.get(j);
				int[] box = p.getBox();
				int xBoxDistance = Math.abs(pointBox[0] - box[0]);
				int yBoyDistance = Math.abs(pointBox[1] - box[1]);
				if (yBoyDistance <= 1 && xBoxDistance <= 2) {
					double d = point.distance(p);
					if (d < distance)
						distance = d;
					continue;
				} else
					break;

			}

		}

	}

	private void SetEachPointInBox() {

		// set boxes to points;
		for (Point point : sortedPointInRange) {
			int xbox = getXBoxPosition(point.getX(), point.isLeft(), limit,
					halfDelta);
			int ybox = (int) Math.round(point.getY() / halfDelta); // OJO

			point.setBox(xbox, ybox);
		}

	}

	private void getPointsInRange(Point leftLast) {

		int leftLastIndex = sortedPointList.indexOf(leftLast);
		int rightFirstIndex = leftLastIndex + 1;

		while (true) {
			if (leftLastIndex == -1)
				break;
			Point point = sortedPointList.get(leftLastIndex);
			double xValue = point.getX();
			if (limit - xValue <= delta) {
				point.setOrderByY();
				pointInRange.add(point);
				leftLastIndex--;
			} else
				break;
		}

		while (true) {
			if (rightFirstIndex >= sortedPointList.size() - 1)
				break;
			Point point = sortedPointList.get(rightFirstIndex);
			double xValue = point.getX();
			if (xValue - limit <= delta) {
				point.setOrderByY();
				pointInRange.add(point);
				rightFirstIndex++;
			} else
				break;
		}
	}

	private double getShortestDistanceFromBothSides(int lastLeftIndex) {

		double shortestDistance = Double.POSITIVE_INFINITY;
		for (int i = 0; i < sortedPointList.size() - lastLeftIndex - 1; i++) {
			Point point = sortedPointList.get(i);
			for (int j = 0; j < sortedPointList.size() - lastLeftIndex - 1; j++) {
				Point nextPoint = sortedPointList.get(j);
				if (!point.equals(nextPoint)) {
					double distance = point.distance(nextPoint);
					if (shortestDistance > distance) {
						nameFrom = point.getName();
						nameTo = nextPoint.getName();
						shortestDistance = distance;
					}

				}
			}

			point.setLeft();
		}

		for (int i = lastLeftIndex + 1; i <= sortedPointList.size() - 1; i++) {
			Point point = sortedPointList.get(i);
			for (int j = lastLeftIndex + 1; j <= sortedPointList.size() - 1; j++) {
				Point nextPoint = sortedPointList.get(j);
				if (!point.equals(nextPoint)) {
					double distance = point.distance(nextPoint);
					if (shortestDistance > distance) {
						nameFrom = point.getName();
						nameTo = nextPoint.getName();
						shortestDistance = distance;
					}

				}
			}
		}

		return shortestDistance;

	}

	private int getXBoxPosition(double x, boolean left, double limit,
			double halfDelta) {
		if (left) {
			if (limit - x <= halfDelta)
				return 2;
			else
				return 1;
		} else {
			if (x - limit <= halfDelta)
				return 3;
			else
				return 4;
		}
	}

	private void OrderPointsByX() {
		Collections.sort(pointList, new Comparator<Point>() {

			public int compare(Point o1, Point o2) {

				Double x1 = ((Point) o1).getX();
				Double x2 = ((Point) o2).getX();
				int sComp = x1.compareTo(x2);

				if (sComp != 0) {
					return sComp;
				} else {
					Double y1 = ((Point) o1).getY();
					Double y2 = ((Point) o2).getY();
					return y1.compareTo(y2);
				}
			}
		});

		sortedPointList = pointList;
	}

	private void OrderPointsByY() {
		Collections.sort(pointInRange, new Comparator<Point>() {

			public int compare(Point o1, Point o2) {

				Double x1 = ((Point) o1).getY();
				Double x2 = ((Point) o2).getY();
				int sComp = x1.compareTo(x2);

				if (sComp != 0) {
					return sComp;
				} else {
					Double y1 = ((Point) o1).getX();
					Double y2 = ((Point) o2).getX();
					return y1.compareTo(y2);
				}
			}
		});

		sortedPointInRange = pointInRange;
	}

	private void GetDataFromFile(Scanner scan) {
		String available;

		while (scan.hasNextLine()) {

			available = scan.nextLine();

			if (available == null)
				continue;

			String[] pointInfo = available.split(" ");

			if (pointInfo.length < 3)
				continue;

			Point newPoint = new Point(pointInfo[0],
					Double.parseDouble(pointInfo[1]),
					Double.parseDouble(pointInfo[2]));
			pointList.add(newPoint);
		}
	}

	private void GetDataFromTSPFile(Scanner scan) {
		String available;

		while (scan.hasNextLine()) {

			available = scan.nextLine();
			if (available.startsWith("EOF"))
				break;

			if (available.isEmpty())
				continue;

			String[] pointInfo = available.split(" ");

			pointInfo = RemoveSpaceFromArray(pointInfo);

			if (pointInfo.length < 3)
				continue;

			try  
			{  
				double d = Double.parseDouble(pointInfo[0]);  
			}  
			catch(NumberFormatException nfe)  
			{  
				continue;
			}  

			Point newPoint = new Point(pointInfo[0],
					Double.parseDouble(pointInfo[1]),
					Double.parseDouble(pointInfo[2]));
			pointList.add(newPoint);
		}

	}

	private String[] RemoveSpaceFromArray(String[] pointInfo) {

		ArrayList<String> list = new ArrayList<String>();

		for (String s : pointInfo) {
			if (s != null && s.length() > 0) {
				list.add(s);
			}
		}

		pointInfo = list.toArray(new String[list.size()]);

		return pointInfo;

	}
}
