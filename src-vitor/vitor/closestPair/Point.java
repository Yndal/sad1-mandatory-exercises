package vitor.closestPair;
import java.util.Comparator;


public class Point implements Comparable<Point> {
	private final String name;
	private final double x;
	private final double y;
	private boolean left;
	private boolean last;
	private double distanceToNextPoint;
	private int[] box = new int[2];
	
	private boolean orderByY;
	
	public Point(String pointName, double xCoord, double yCoord) {
		name = pointName;
		x = xCoord;
		y= yCoord;				
	}	
	
	public double getX() {
		return x;
	}
	
	public double  getY() {
		return y;
	}
	
	public String getName() {
		return name;
	}
	
	public void setLast() {
		last = true;
	}
		
	public void setLeft() {
		left = true;
	}
	
	public boolean isLeft() {
		return left;				
	}
	
	public void setBox(int x, int y) {
		box[0] = x;
		box[1] = y;
	}
	
	public int[] getBox() {
		return box;
	}
	
	public void setNullDistance() {
		distanceToNextPoint = -1;
	}
	
	public boolean isLast() {
		return last;
	}
	
	 public double distance(Point p)
	    {
		    double dX = getX() - p.getX();
	        double dY = getY() - p.getY();
	        distanceToNextPoint = Math.sqrt(dX * dX + dY * dY);
	        return distanceToNextPoint;
	    }
	 	
	public void setOrderByY() {
		orderByY = true;
	}
	
	@Override
	public int compareTo(Point other) {
		if(orderByY) return (int)y - (int)other.y;
		return (int)(x - other.x);
	}
	
}
