package petru.closestPair;

import java.awt.Point;

public class NaiveAlgorithm {
	public static Point[] getClosestPair(Point[] points) {
	    double min = Double.MAX_VALUE;

	    Point[] closestPair = new Point[2];

	    for (Point p1 : points) {
	        for (Point p2 : points) {
	            if (p1 != p2) {
	                double dist = p1.distance(p2);

	                if (dist < min) {
	                    min = dist;

	                    closestPair[0] = p1;
	                    closestPair[1] = p2;
	                }
	            }
	        }
	    }

	    return closestPair;
	}
}
