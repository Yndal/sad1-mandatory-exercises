package petru.closestPair;

import java.awt.Point;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class ClosestPairsApplication {
    public static void main(String[] args) throws FileNotFoundException {
    	if (args == null || args.length != 2) {
    		System.out.println("Application should be called using next syntax");
    		System.out.println("ClosestPairsApplication naive [complete filename]");
    		System.out.println("or");
    		System.out.println("ClosestPairsApplication dac [complete filename]");
    		return;
    	}
    	
		List<Point[]> inputPoints = getPointList(args[1]);
		if (inputPoints == null) {
    		System.out.println("File is not valid ! Please check the input path");
    		return;
		}
		
		if (inputPoints.size() == 0) {
    		System.out.println("File could be found, but no points could be successfully parsed. Please check the content");
    		return;
		}
			
    	if (args[0] == "naive") {
    		Point[] selectedPoint = NaiveAlgorithm.getClosestPair((Point[]) inputPoints.toArray());
    		System.out.println("Selected point is: " + selectedPoint[0] + " " + selectedPoint[1]);
    	}
    }

	private static List<Point[]> getPointList(String filePath) {
		Scanner fileScanner = null;
    	List<Point[]> inputPoints = null;

    	if (isValidFile(filePath)) {
    		fileScanner = getFile(filePath);
    	   	return loadData(fileScanner);
    	}

    	return inputPoints;
	}
    
    
	/*
	 * Internal function - check file
	 * */
    private static Boolean isValidFile(String inputFile) {
    	if (inputFile.isEmpty())
    		return false;

    	File checkFile = new File(inputFile);
    	return (checkFile.exists() && !checkFile.isDirectory());
    }

	/*
	 * Open the file and create a valid Scanner object (this will be closed once the data is loaded)
	 * */
    private static Scanner getFile(String inputFile) {
    	Scanner scanner = null;
    	try {
			scanner = new Scanner(new BufferedInputStream(new FileInputStream(inputFile)));
		} catch (IOException e) {
			System.err.println("Application could not read file: " + inputFile);
			return null;
		}
    	
    	return scanner;
    }
    
	/*
	 * Load edge collection from the file having next structure
	 * */
    private static List<Point[]> loadData(Scanner fileScanner) {
		List<Point[]> pointList = new ArrayList<Point[]>();
    	
    	if (fileScanner != null) {
    		while(fileScanner.hasNext()){
    			Point[] newPoint = getPoint(fileScanner.nextLine());
    			if (newPoint != null)
    				pointList.add(newPoint);	
    		}
    		
    		fileScanner.close();
    	}
    	
    	return pointList;
    }

	/*
	 * Parse a valid line from file (e.g. "San Diego"--"San Antonio" [1319])
	 * */
	private static Point[] getPoint(String textLine) {
		Point[] newPoint = new Point[2];
		
		/*
		if(textLine.contains("--") && textLine.contains("[") && textLine.contains("]")) {
			String cities = textLine.split("\\[")[0];
			String cityFrom = cities.split("--")[0].replace('"', ' ').trim();
			String cityTo = cities.split("--")[1].replace('"', ' ').trim();
			int distance = Integer.valueOf(textLine.split("\\[")[1].replace(']', ' ').trim());
			
			if (cityFrom != cityTo && distance > 0)
				newElement = new EdgeElement(cityFrom, cityTo, distance); 
		}
		*/
		
		return newPoint;
	}
}
