package petru.gorillaVsSeaCucumber;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

public class DNA_Alignment {
    static LinkedList<Character> sequence_1 = new LinkedList<>();
    static LinkedList<Character> sequence_2 = new LinkedList<>();
    static int[][] M;
    static int[][] costMatrix;
    static Map<String, Integer> charToNumber = new HashMap<>();
    
    static Map<String, String> map = new HashMap<>();

    static Boolean parseMatrixOfScores(String filePath) throws FileNotFoundException {
    	File file = new File(filePath);
        if (!file.exists()) {
        	System.out.println("Input file provided " + filePath + " is not valid !");
        	return false;	
        }
        	
        Scanner sc = new Scanner(file);

        //skip comments
        for (int i = 0; i < 6; i++) {
            sc.nextLine();
        }

        String lineOfLetters = sc.nextLine();
        String[] letters = lineOfLetters.split("\\s+");

        int dimension = letters.length;
        if (dimension > 0) {
	        costMatrix = new int[dimension][dimension];
	
	        for (int i = 0; i < letters.length; i++) {
	            charToNumber.put(letters[i], i);
	        }
	
	        while (sc.hasNextLine()) {
	            String line = sc.nextLine();
	            String[] weights = line.split("\\s+");
	
	            int index = charToNumber.get(weights[0]);
	
	            for (int i = 1; i < weights.length; i++) {
	                costMatrix[index][i] = Integer.parseInt(weights[i]);
	            }
	        }
        }
        
        sc.close();
        
        return (dimension > 0);
    }
    
    public static void printArray(int matrix[][]) {
        for (int[] row : matrix) 
            System.out.println(Arrays.toString(row));       
    }    
    
    public static Boolean parseProteinSequence(String inputFile) throws FileNotFoundException {
        File file = new File(inputFile); 
        if (file == null || !file.exists()) {
        	System.out.println("Input species file provided " + inputFile + " is not valid !");
        	return false;	
        }
        
        Scanner sc = new Scanner(file);
        
        String available = null;
        while (sc.hasNextLine()) {

			if (available == null)
				available = sc.nextLine();
				
			String name = null;
			if (available.startsWith(">")) {
				String[] spliName = available.split(" ");
				name = spliName[0].substring(1);
				//newSpacies.setName(name);
			}

			String proteins = "";
			available = sc.nextLine();
			while (!available.startsWith(">")) {
				proteins += (available);
				if (sc.hasNext())
					available = sc.nextLine();
				else
					break;
			}

	        map.put(name, proteins);
		}
        
        
        sc.close();
        
        return (map.size() > 0);
    }

      
    public static int sequenceAlignment(int m, int n, String specieOne, String specieTwo) {
        M = new int[m + 1][n + 1];

        for (int i = 1; i <= m; i++) {
            M[i][0] = i * -4;
        }
        for (int j = 1; j <= n; j++) {
            M[0][j] = j * -4;
        }
        // for debug
        // printArray(M);
        
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                String charSpecieOne = Character.toString(specieOne.charAt(i - 1));
                String charSpecieTwo = Character.toString(specieTwo.charAt(j - 1));
                
                int charAsIntSpecieOne = charToNumber.get(charSpecieOne);
                int charAsIntSpecieTwo = charToNumber.get(charSpecieTwo);
                
                int cost = costMatrix[charAsIntSpecieOne][charAsIntSpecieTwo];

                int optionOne = cost + M[i - 1][j - 1];
                int optionTwo = -4 + M[i - 1][j];
                int optionThree = -4 + M[i][j - 1];
             
                //set M[i][j] to option with max value
                M[i][j] = Math.max(Math.max(optionOne, optionTwo), optionThree);
            }
        }
        return M[m][n];
    }

    //recursive helper method for printSolution
    //takes time n + m
    static void traverseSolution(int i, int j, String specieOne, String specieTwo) {

        //basecase
        if (i == 0 || j == 0)
            return;

        String charSpecieOne = Character.toString(specieOne.charAt(i - 1));
        String charSpecieTwo = Character.toString(specieTwo.charAt(j - 1));

        int charAsIntSpecieOne = charToNumber.get(charSpecieOne);
        int charAsIntSpecieTwo = charToNumber.get(charSpecieTwo);

        int cost = costMatrix[charAsIntSpecieOne][charAsIntSpecieTwo];

        int optionOne = cost + M[i - 1][j - 1];
        int optionTwo = -4 + M[i - 1][j];
        int optionThree = -4 + M[i][j - 1];

        int maxValue = Math.max(Math.max(optionOne, optionTwo), optionThree);

        if (maxValue == optionOne) {
            sequence_1.addFirst(specieOne.charAt((i--) - 1));
            sequence_2.addFirst(specieTwo.charAt((j--) - 1));
        } else if (maxValue == optionThree) {
            sequence_1.addFirst('-');
            sequence_2.addFirst(specieTwo.charAt((j--) - 1));
        } else {
            sequence_1.addFirst(specieOne.charAt((i--) - 1));
            sequence_2.addFirst('-');
        }
        traverseSolution(i, j, specieOne, specieTwo);
    }

    public static void printSolution(int i, int j, String specieOne, String specieTwo, int result) {
        sequence_1.clear();
        sequence_2.clear();

        traverseSolution(i, j, specieOne, specieTwo);

        //System.out.println(specieOne + " - " + specieTwo + " : " + result);
        System.out.println("result : " + result);

        for (Character ch : sequence_1) {
            System.out.print(ch);
        }

        System.out.println("\n");

        for (Character ch : sequence_2) {
            System.out.print(ch);
        }

        System.out.println("\n---------------------------------------");
    }

    public static void main(String[] args) throws FileNotFoundException {
        if (!parseMatrixOfScores("C:\\Users\\riatec\\Documents\\Git\\mandatory_exercises\\mandatory_exercises\\data-in\\BLOSUM62.txt")) {
        	System.out.println("Matrix of scored could not be parsed !");
        	return;
        }
        
        if (!parseProteinSequence("C:\\Users\\riatec\\Documents\\Git\\mandatory_exercises\\mandatory_exercises\\data-in\\HbB_FASTAs.in")){
        //if (!parseProteinSequence("C:\\Users\\riatec\\Documents\\Git\\mandatory_exercises\\mandatory_exercises\\data-in\\Toy_FASTAs.in")) {
        	System.out.println("Input protein sequence file could not be parsed !");
        	return;
        }
        
        // debug: check output
        // printArray(costMatrix);
        
        int index = 0;
        for (Entry<String, String> entry: map.entrySet()) {
        	String specie = entry.getKey();
            String dna = entry.getValue();
            
        	// System.out.println("Debug: Specie: " + specie + " dna " + dna);	

            int otherIndex = 0;
            for (String otherSpecie: map.keySet()) {
            	// System.out.println("Debug: Other specie: " + otherSpecie);	
                
                if (otherSpecie.equals(specie))
                    continue;

            	// System.out.println("Debug: Other index: " + otherIndex + " - index: " + index);	
                if (otherIndex++ < index)
                    continue;
                
                String dnaOther = map.get(otherSpecie);
            	// System.out.println("Debug: Other dna: " + dnaOther);	

            	int result = sequenceAlignment(dna.length(), dnaOther.length(), dna, dnaOther);
                
                System.out.println("specie: " + specie + " - otherSpecie: " + otherSpecie);
                printSolution(dna.length(), dnaOther.length(), dna, dnaOther, result);
            }

            index++;          
        }
    }
}
