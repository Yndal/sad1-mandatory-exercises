package petru.behindEnemyLines;

import java.util.Stack;
import petru.behindEnemyLines.FordFulkersonAlgorithm.FordFulkersonData;

public class BreadthFirstAlgorithm {
    private static final int INFINITY = Integer.MAX_VALUE;
    private boolean[] marked;  			// marked[v] = is there an s->v path?
    private Edge[] edgeTo;				// list of edges pointing to the selectedNode / index
    private int[] capacities;			// capacity

    public BreadthFirstAlgorithm(FordFulkersonData mf, int s) {
        marked = new boolean[mf.numberOfNodes];
        capacities = new int[mf.numberOfNodes];
        edgeTo = new Edge[mf.numberOfNodes];
        for (int v = 0; v < mf.numberOfNodes; v++) {
            capacities[v] = INFINITY;
        }
        
        run(mf, s);
    }

    // BFS from single source
    private void run(FordFulkersonData mf, int s) {
        EdgeQueue<Integer> queue = new EdgeQueue<Integer>();
        marked[s] = true;
        capacities[s] = 0;
        queue.enqueue(s);
        
        while (!queue.isEmpty()) {
            int v = queue.dequeue();

            for (Edge e : mf.adj[v]) {
                if (e.capacity == 0) {
                    continue;
                }

                if (!marked[e.to]) {
                    edgeTo[e.to] = e;
                    capacities[e.to] = capacities[v] + 1;
                    marked[e.to] = true;
                    queue.enqueue(e.to);
                }
            }
        }
    }

    // length of shortest path from s (or sources) to v
    public int distTo(int v) {
        return capacities[v];
    }

    // is there a directed path from s (or sources) to v?
    public boolean hasPathTo(int v) {
        return marked[v];
    }

    // shortest path from s (or sources) to v; null if no such path
    public Iterable<Integer> getPathTo(int destination) {
        if (!hasPathTo(destination)) {
            return null;
        }

        Stack<Integer> path = new Stack<Integer>();
        int x;
        for (x = destination; capacities[x] != 0; x = edgeTo[x].from) {
            path.push(x);
        }
        path.push(x);
        return path;
    }

    // shortest path - providing edges
    public Iterable<Edge> getPathEdges(int destination) {
        if (!hasPathTo(destination)) {
            return null;
        }
        Stack<Edge> edges = new Stack<>();
        int x;
        for (x = destination; capacities[x] != 0; x = edgeTo[x].from) {
            edges.push(edgeTo[x]);
        }
        return edges;
    }

    // get bottleneck = as minimum capacity along the path
    public int getBottleneck(int dest) {
        int min = INFINITY;
        for (Edge e : getPathEdges(dest)) {
            if (e.capacity != -1 && e.capacity < min) {
                min = e.capacity;
            }
        }
        return min;
    }
}
