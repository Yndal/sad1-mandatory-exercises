package petru.behindEnemyLines;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class FordFulkersonAlgorithm {
	public static class FordFulkersonData {
	    static String[] nodes = null;
	    int numberOfArcs = 0;
	    int numberOfNodes = 0;
	    List<Edge>[] adj = null;
	    
	    public int nodeCount() {
	    	return numberOfNodes;
	    }
	    
	    public String[] getNodes() {
	    	return nodes;
	    }
	    
	    public void read(String inputFileName) throws FileNotFoundException {
	        File file = new File(inputFileName);

	        Scanner sc = new Scanner(file);

	        String countOfNodesString = sc.nextLine().trim();	
	        numberOfNodes = Integer.parseInt(countOfNodesString);
	        adj = new LinkedList[numberOfNodes];
	        nodes = new String[numberOfNodes];

	        for (int i = 0; i < numberOfNodes; i++) {
	            nodes[i] = sc.nextLine().trim();
	            adj[i] = new LinkedList<>();
	        }

	        String countOfArcsString = sc.nextLine().trim();
	        numberOfArcs = Integer.parseInt(countOfArcsString);

	        //build adjacencies matrix
	        for (int k = 0; k < numberOfArcs; k++) {
	            String line = sc.nextLine();
	            String[] tokens = line.split("\\s+");

	            int v = Integer.parseInt(tokens[0]);
	            int w = Integer.parseInt(tokens[1]);

	            int capacity = Integer.parseInt(tokens[2]);

	            adj[v].add(new Edge(v, w, capacity));
	            adj[w].add(new Edge(w, v, capacity));
	        }
	    }
	}
	
	FordFulkersonData data = null;

    public FordFulkersonAlgorithm(FordFulkersonData inputData) {
        data = inputData;
    }
    
    public void augmentingPath(BreadthFirstAlgorithm bfs, int destination) {
        int bottleNeck = bfs.getBottleneck(destination);

        for (Edge edge : bfs.getPathEdges(destination)) {
            edge.flow += bottleNeck;

            if (edge.unbounded) {
                continue;
            }
            edge.capacity -= bottleNeck;

            Iterable<Edge> edges = data.adj[edge.to];
            for (Edge e : edges) {
                if (e.to == edge.from) {
                    e.capacity += bottleNeck;
                }
            }
        }
    }

    public int getMaxFlow(FordFulkersonData inputData) {
        BreadthFirstAlgorithm bfs = new BreadthFirstAlgorithm(inputData, 0);
        if (bfs.hasPathTo(data.nodeCount() - 1)) {
            augmentingPath(bfs, data.nodeCount() - 1);
            getMaxFlow(inputData);
        }

        int flow = 0;
        for (Edge e : data.adj[0]) {
            flow += e.flow;
        }

        return flow;
    }
    
    public Iterable<Edge> minCut(FordFulkersonData inputData, String fileName) throws FileNotFoundException {
        BreadthFirstAlgorithm bfs = new BreadthFirstAlgorithm(inputData, 0);
        
        Set<Integer> s_set = new HashSet<>();
        Set<Integer> t_set = new HashSet<>();
        
        for (int i = 1; i < inputData.numberOfNodes; i++) {
            if (bfs.hasPathTo(i))
                s_set.add(i);
            else
                t_set.add(i);
        }
        
        inputData.read(fileName);
        
        List<Edge> minCutEdges = new ArrayList<>();
        for (int i: s_set) {
            for (Edge e: inputData.adj[i]) {
                if ( t_set.contains(e.to) )
                    minCutEdges.add(e);
            }   
        }
        
        return minCutEdges;
    }
}
    	