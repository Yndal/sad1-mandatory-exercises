package petru.behindEnemyLines;

import java.io.FileNotFoundException;
import petru.behindEnemyLines.FordFulkersonAlgorithm.*;

public class FordFulkersonApplication {
    public static void main(String[] args) throws FileNotFoundException {
    	FordFulkersonData inputData = getInputData(args);
    	
    	FordFulkersonAlgorithm algorithmInstance = new FordFulkersonAlgorithm(inputData);
        int maxFlowCount = algorithmInstance.getMaxFlow(inputData);

        System.out.println("Maximum flow of graph: " + maxFlowCount);
        System.out.println("-----------------------------");
        
        System.out.println("Edges of the minimum cut are:");
        int minCapacity = 0;
        for (Edge e: algorithmInstance.minCut(inputData, getFileName(args))) {
            System.out.println("\tEdge (id: " + e + ") \t\tEdge nodes: " + inputData.getNodes()[e.from] +" - " + inputData.getNodes()[e.to] );
            minCapacity += e.capacity;
        }
        
        if (minCapacity == 0)
            System.out.println("\tThere is no minimum cut - full input capacity could be transferred via graph !");
        else
        	System.out.println("\tCapacity of minimum cut: "			 + minCapacity);

        System.out.println("Algorithm ends");
    }

	private static FordFulkersonData getInputData(String[] args)
			throws FileNotFoundException {
		String inputFile = getFileName(args);
    	FordFulkersonData inputData = new FordFulkersonData();
    	inputData.read(inputFile);
		return inputData;
	}

	private static String getFileName(String[] args) {
		return (args != null && args.length > 0 && args[0] != "") 
					? args[0] 
							: "C:\\_Work\\sad1-mandatory-exercises\\data-in\\rail.txt";
	}
}
