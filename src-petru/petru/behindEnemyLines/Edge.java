package petru.behindEnemyLines;

public class Edge {
    int from, to, capacity;
    int flow;
    final boolean unbounded;
    
    public Edge(int i, int j, int capacity) {
        this.from = i;
        this.to = j;
        this.capacity = capacity;
        if (capacity == -1)
            unbounded = true;
        else
            unbounded = false;
    }
    
    public Edge(int i, int j, int capacity, int flow) {
        this(i, j, capacity);
        this.flow = flow;
    }
    
    @Override
    public String toString() {
        return from + " - " + to + " : " + capacity;
    }
}