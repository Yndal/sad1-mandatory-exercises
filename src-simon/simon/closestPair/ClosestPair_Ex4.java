package simon.closestPair;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JOptionPane;


public class ClosestPair_Ex4 {

static String txtFilename;
final static int X = 0, Y = 1;
static int iP, nP;
static String txtWorkingDir = ""; //"C:\\Users\\Simon\\Desktop\\Java Eclipse\\SAD1 Data\\lab4\\";
static String[] txtFilenames = {
"a280.tsp", "ali535.tsp", "att48.tsp", "att532.tsp", "berlin52.tsp", "bier127.tsp", "brd14051.tsp", "burma14.tsp",
"ch130.tsp", "ch150.tsp", 
"close-pairs-1.in", "close-pairs-2.in", "close-pairs-3.in", "close-pairs-4.in", "close-pairs-5.in", "close-pairs-6.in",
"d198.tsp", "d493.tsp", "d657.tsp", "d1291.tsp", "d1655.tsp", "d2103.tsp",
"d15112.tsp", "d18512.tsp", "dsj1000.tsp", "eil51.tsp", "eil76.tsp", "eil101.tsp", "fl417.tsp", "fl1400.tsp",
"fl1577.tsp", "fl3795.tsp", "fnl4461.tsp", "gil262.tsp", "gr96.tsp", "gr137.tsp", "gr202.tsp", "gr229.tsp", "gr431.tsp", "gr666.tsp", 
"kroA100.tsp", "kroA150.tsp", "kroA200.tsp", "kroB100.tsp", "kroB150.tsp", "kroB200.tsp", "kroC100.tsp", "kroD100.tsp", "kroE100.tsp", "lin105.tsp", 
"lin318.tsp", "linhp318.tsp", "nrw1379.tsp", "p654.tsp", "pcb442.tsp", "pcb1173.tsp", "pcb3038.tsp", "pla7397.tsp", "pla33810.tsp", "pla85900.tsp", 
"pr76.tsp", "pr107.tsp", "pr124.tsp", "pr136.tsp", "pr144.tsp", "pr152.tsp", "pr226.tsp", "pr264.tsp", "pr299.tsp", "pr439.tsp", 
"pr1002.tsp", "pr2392.tsp", "rat99.tsp", "rat195.tsp", "rat575.tsp", "rat783.tsp", "rd100.tsp", "rd400.tsp", "rl11849.tsp"
};

public static void main(String[] args) throws FileNotFoundException {
System.out.println("Working started...   Pair-Pair   nPairs    distance");
int iFileUser = getFileNo();
if (iFileUser < 0){
for (int iFile = 0; iFile < txtFilenames.length; iFile++) {processFile(iFile);}
}else{
processFile(iFileUser);
}
System.out.println("Done :-)");
}


public static void processFile(int iFile) throws FileNotFoundException {
txtFilename = txtFilenames[iFile];

File inputFile = new File(txtWorkingDir + txtFilename);
Scanner inData = new Scanner(inputFile);

String txtLine;
char ch0;
ArrayList<Integer> Pno = new ArrayList<Integer>();
ArrayList<Double> Px = new ArrayList<Double>();
ArrayList<Double> Py = new ArrayList<Double>();

String[] txtPset = new String[2]; // s=0 : Pn , s=1 : Px , s=2 : Py
iP = 0;

while(inData.hasNextLine()) 
{
txtLine = inData.nextLine().trim();
if (txtLine.length() >= 5) {
ch0 = txtLine.charAt(0);
if(ch0 >= '0' && ch0 <= '9'){
txtPset = getWords(txtLine);
if (txtPset.length == 3) {
Pno.add(Integer.parseInt(txtPset[0]));
Px.add(Double.parseDouble(txtPset[1]));
Py.add(Double.parseDouble(txtPset[2]));
iP++;
}
}
}
}
inData.close();
nP = iP;

double[][] P = new double[nP][2];
int[] Pn = new int[nP];
for (int i=0; i < nP; i++){
P[i][0] = Px.get(i);
P[i][1] = Py.get(i);
Pn[i] = Pno.get(i);
}

mainSub(P, Pn);
System.out.println("../data/" + txtFilename + ":    " + dMinPair[X] + "-" + dMinPair[Y] + "     " + nP + "     " + dMin);
}

// ************ MAIN **************
static int[] dMinPair = new int[2]; // "[2]" is the number of dimensions: x & y
static double dMin;
static double d;

public static void mainSub(double[][] P, int[] Pn){
dMin = Double.MAX_VALUE;
sortP(P, Pn, X);
findPair(P, Pn);

double[][] S = new double[P.length][2];
int[] Sn = new int[P.length];
sortP(S, Sn, Y);

for (int i = 0; i < nP; i++){
for (int j = Math.max(0, i - 7); j <= Math.min(nP - 1, i + 7); j++){ //"15 (-7 -> 7)" see s135
if (i == j) {continue;}
d = getD(P, i, j);
if (d < dMin) {
dMin = d; dMinPair[X] = Pn[i]; dMinPair[Y] = Pn[j];
}
}
}
}

public static void findPair(double[][] P, int[] Pn){
if (P.length <= 3) {
int lP = P.length;
for (int i = 0; i < lP - 1; i++){
for (int j = i + 1; j < lP; j++){
d = getD(P, i, j);
if (d < dMin) {
dMin = d;
dMinPair[X] = Pn[i];
dMinPair[Y] = Pn[j];
}
}
}
return;
}
 
double[][] Q = new double[P.length / 2][2]; // "[2]" is the number of dimensions: x & y
double[][] R = new double[P.length - Q.length][2];

for (int i=0; i < Q.length; i++) {
Q[i][X] = P[i][X];
Q[i][Y] = P[i][Y];
}
int lQ = Q.length;
for (int i=0; i < R.length; i++) {
R[i][X] = P[i + lQ][X];
R[i][Y] = P[i + lQ][Y];
}

findPair(Q, Pn);
findPair(R, Pn);
mergeQR(Q, R, P);
}

private static void mergeQR(double[][] Q, double[][] R, double[][] P){
int iQ = 0, iR = 0;
int j = 0;

while(iQ < Q.length && iR < R.length){
if (Q[iQ][0] < R[iR][0]){ // 0 is tmp
P[j][X] = Q[iQ][X];
P[j][Y] = Q[iQ][Y];
iQ++;
}else{
P[j][X] = R[iR][X];
P[j][Y] = R[iR][Y];
iR++;
}
j++;
}
while(iQ < Q.length){
P[j][X] = Q[iQ][X];
P[j][Y] = Q[iQ][Y];
iQ++; j++;
}
while(iR < R.length){
P[j][X] = R[iR][X];
P[j][Y] = R[iR][Y];
iR++; j++;
}
}

private static double getD(double[][] P, int i, int j){
return Math.sqrt(Math.pow(P[i][X]-P[j][X], 2) + Math.pow(P[i][Y]-P[j][Y], 2));
}
//private static double getD(double[][] Q, double[][] R, int i, int j){
// return Math.sqrt(Math.pow(Q[i][x]-R[j][x], 2) + Math.pow(Q[i][y]-R[j][y], 2));
//}


// ************ END **************



public static void printP(double[][] Pin, int[] Pnin){
System.out.println();
for (int i=0; i < Pin.length; i++){
System.out.println("i+1  Pnin[i]  Pin[i][X]  Pin[i][Y]:  " + (i + 1) + "  " + Pnin[i] + "  " + Pin[i][X] + "  " + Pin[i][Y]);
}
}


/**
* Merge Sort - s624
* @param Pin Array to sort 
* @param sxy sxy = 0 then sort by x : sxy = 1 then sort by y
*/
public static void sortP(double[][] Pin, int[] Pnin, int sxy){
if (Pin.length <= 1) {return;}

double[][] Pf = new double[Pin.length / 2][2];
double[][] Ps = new double[Pin.length - Pf.length][2];

int[] Pnf = new int[Pnin.length / 2];
int[] Pns = new int[Pnin.length - Pnf.length];

for (int i=0; i < Pf.length; i++) {
Pf[i][0] = Pin[i][0];
Pf[i][1] = Pin[i][1];
Pnf[i] = Pnin[i];
}
int lP1 = Pf.length;
for (int i=0; i < Ps.length; i++) {
Ps[i][0] = Pin[i + lP1][0];
Ps[i][1] = Pin[i + lP1][1];
Pns[i] = Pnin[i + lP1];
}
sortP(Pf, Pnf, sxy);
sortP(Ps, Pns, sxy);
mergeP(Pf, Ps, Pin, Pnf, Pns ,Pnin, sxy);
}

private static void mergeP(double[][] Pf, double[][] Ps, double[][] Pin, int[] Pnf, int[] Pns, int[] Pnin, int sxy){
int iF = 0, iS = 0;
int j = 0;

while(iF < Pf.length && iS < Ps.length){
if (Pf[iF][sxy] < Ps[iS][sxy]){
Pin[j][0] = Pf[iF][0];
Pin[j][1] = Pf[iF][1];
Pnin[j] = Pnf[iF];
iF++;
}else{
Pin[j][0] = Ps[iS][0];
Pin[j][1] = Ps[iS][1];
Pnin[j] = Pns[iS];
iS++;
}
j++;
}
while(iF < Pf.length){
Pin[j][0] = Pf[iF][0];
Pin[j][1] = Pf[iF][1];
Pnin[j] = Pnf[iF];
iF++; j++;
}
while(iS < Ps.length){
Pin[j][0] = Ps[iS][0];
Pin[j][1] = Ps[iS][1];
Pnin[j] = Pns[iS];
iS++; j++;
}
}



public static int getFileNo()
{
String txtToUser = "Type -1 for all files or a number between 1 and " + txtFilenames.length + " for a specific file";
String txtFromUser = JOptionPane.showInputDialog(txtToUser); //s138
 
Scanner in = new Scanner(txtFromUser);
 
int i = -1;
if (in.hasNextInt()) {i = Integer.parseInt(txtFromUser) - 1;}
if (i < 0) i = -1;
if (i >= txtFilenames.length) i = txtFilenames.length - 1;
 
in.close();
return i;
}


public static String[] getWords(String str)
{
str = str + " "; //not pretty!
int lStr = str.length();
int nWord = 0;
boolean fFindStart = true; //str.charAt(0) == ' ';

for(int i = 0; i < lStr; i++) {
if (fFindStart) {
if (str.charAt(i) != ' ') {nWord++; fFindStart = false;}}
else {
if (str.charAt(i) == ' ') {fFindStart = true;}}
}

String[] strWords = new String[nWord];
//--
int iWord = 0;
int a = 0, b = 0; //start value don't matter
fFindStart = true;

for(int i = 0; i < lStr; i++) {
if (fFindStart) {
if (str.charAt(i) != ' ') {a = i; fFindStart = false;}}
else {
if (str.charAt(i) == ' ') {b = i; fFindStart = true; strWords[iWord++] = str.substring(a, b);}}
}
return strWords;
}
}