package simon.gorillaVsSeaCucumber;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JOptionPane;


public class GorillaOrCucumber {

	static String txtFilename;
	static String txtWorkingDir = "data-in\\";
	static String[] txtFilenames = {"Toy_FASTAs.in", "HbB_FASTAs.in"};
	static String txtLine;
	static int[][] pScore = new int[20+4][20+4];
	static char[] chL = new char[20+4];
	static String[][] txtSeqs;

	static String txtSeqI = "";
	static String txtSeqJ = "";

	public static void main(String[] args) throws FileNotFoundException {
		readScoreTab();
		printScoreTab(); //**
		txtSeqs = readProteinSeqs();
		printProteinSeqs(txtSeqs); //**

		// ***Test

		int SpecA = 0, SpecB = 12; //0 = Human : 12 = Sea-Cucumber
		if (txtSeqs[0].length >= 12) {
			System.out.println('\n' + "SpecA: " + txtSeqs[0][SpecA] + ':' + SpecA + "   SpecB: "  + txtSeqs[0][SpecB] + ':' + SpecB);
			txtSeqI = txtSeqs[1][SpecA]; txtSeqJ = txtSeqs[1][SpecB];

			//System.out.println(txtSeqI); System.out.println(txtSeqJ);

			String[] txt = findAlignment(txtSeqI, txtSeqJ);

			System.out.println(txtSeqs[0][SpecA] + "--" + txtSeqs[0][SpecB] + ": " + M[0][0]);
			System.out.println(txt[0]); System.out.println(txt[1]);
		}


		System.out.println();
		for (int iSpecA = 0; iSpecA < txtSeqs[1].length - 1; iSpecA++){
			for (int iSpecB = iSpecA + 1; iSpecB < txtSeqs[1].length; iSpecB++){
				txtSeqI = txtSeqs[1][iSpecA];
				txtSeqJ = txtSeqs[1][iSpecB];
				String[] txts = findAlignment(txtSeqI, txtSeqJ);
				System.out.println(txtSeqs[0][iSpecA] + "--" + txtSeqs[0][iSpecB] + ": " + M[0][0]);
				System.out.println(txts[0]);
				System.out.println(txts[1]);
			}
		}
	}

	static int[][] M;
	static boolean[][] fM;
	static byte[][] dM;

	static int delta;
	public static String[] findAlignment(String txt1, String txt2){
		delta = pScore[0][pScore[0].length - 1];
		int i = txt1.length(); int j = txt2.length();
		M = new int[i+1][j+1];
		fM = new boolean[i+1][j+1];
		dM = new byte[i+1][j+1];

		for (int i2 = 0; i2 <= i; i2++){
			for (int j2 = 0; j2 <= j; j2++){
				fM[i2][j2] = false;
			}
		}
		OPT2(i, j, 0);

		//System.out.println(M[1][1]);

		int iTrace = 0, jTrace = 0;
		String txtAligI = "", txtAligJ = "";
		while(iTrace < i || jTrace < j){

			switch (dM[iTrace][jTrace]){
			case 11 :
				txtAligI += txtSeqI.charAt(iTrace++);
				txtAligJ += txtSeqJ.charAt(jTrace++);
				break;
			case 01 :
				txtAligI += "-";
				txtAligJ += txtSeqJ.charAt(jTrace++);
				break;
			case 10 :
				txtAligI += txtSeqI.charAt(iTrace++);
				txtAligJ += "-";
				break;
			}
		}

		String[] txtRet = new String[2];
		txtRet[0] = txtAligI;
		txtRet[1] = txtAligJ;
		return txtRet;
	}
	static int s11, s10, s01;

	public static void OPT2(int i, int j, int s){

		if (i > 0 && j > 0) {
			s11 = s + getScore(i, j);
			if (!fM[i-1][j-1] || M[i-1][j-1] < s11){
				fM[i-1][j-1] = true;
				M[i-1][j-1] = s11;
				dM[i-1][j-1] = 11;
				OPT2(i-1, j-1, s11);
			}
		}
		if (i > 0) {
			s10 = s + delta;
			if (!fM[i-1][j] || M[i-1][j] < s10){
				fM[i-1][j] = true;
				M[i-1][j] = s10;
				dM[i-1][j] = 10;
				OPT2(i-1, j, s10);
			}
		}if (j > 0) {
			s01 = s + delta;
			if (!fM[i][j-1] || M[i][j-1] < s01){
				fM[i][j-1] = true;
				M[i][j-1] = s01;
				dM[i][j-1] = 01;
				OPT2(i, j-1, s01);
			}
		}
	}


	public static int getScore(int i, int j){
		char chI = txtSeqI.charAt(i - 1);
		char chJ = txtSeqJ.charAt(j - 1);
		boolean fOkI = false;
		boolean fOkJ = false;
		int iCh, jCh;
		for (iCh = 0; iCh < chL.length; iCh++){
			if (chI == chL[iCh]){
				fOkI = true;
				break;
			}
		}
		for (jCh = 0; jCh < chL.length; jCh++){
			if (chJ == chL[jCh]){
				fOkJ = true;
				break;
			}
		}
		if (fOkI && fOkJ){
			return pScore[iCh][jCh];
		}else{
			System.out.println("Error: Amino acid not found!");
			return -4;
		}
	}


	public static void readScoreTab() throws FileNotFoundException {
		txtFilename = "BLOSUM62.txt";
		File inputFile = new File(txtWorkingDir + txtFilename);
		Scanner inData = new Scanner(inputFile);
		char ch0;
		String[] d;

		int iLine = 0;

		while(inData.hasNextLine()) 
		{
			txtLine = inData.nextLine();

			if (txtLine.length() >= 20) {
				ch0 = txtLine.charAt(0);
				if (ch0 != ' ' && ch0 != '#') {
					if (iLine < pScore.length){
						d = getWords(txtLine);
						chL[iLine] = d[0].charAt(0);
						for (int i = 1; i < d.length; i++){
							pScore[iLine][i-1] = Integer.parseInt(d[i]);
						}
						iLine++;
					}
				}
			}
		}
		inData.close();
	}

	public static void printScoreTab(){
		System.out.print(" ");
		for (int i = 0; i < chL.length; i++){
			System.out.print("  " + chL[i]);
		}
		System.out.println();
		for (int i = 0; i < pScore.length; i++){
			System.out.print(chL[i]);
			for (int i2 = 0; i2 < pScore.length; i2++){
				System.out.printf("%3d", pScore[i][i2]);
			}
			System.out.println();
		}
	}

	public static String[][] readProteinSeqs() throws FileNotFoundException {
		txtFilename = txtFilenames[getFileNo()];
		File inputFile = new File(txtWorkingDir + txtFilename);
		Scanner inData = new Scanner(inputFile);
		char ch0;
		boolean fReadSeq = false;
		String txtSum = "";
		String[] txtS;

		ArrayList<String> txtProName = new ArrayList<String>();
		ArrayList<String> txtProSeq = new ArrayList<String>();

		boolean fContinue = inData.hasNextLine();

		while(fContinue) 
		{
			fContinue = inData.hasNextLine();
			if (fContinue){ txtLine = (inData.nextLine()).trim(); }

			if ((fReadSeq && (txtLine.length() < 1 || txtLine.charAt(0) == '>')) || !fContinue){
				fReadSeq = false;
				txtProSeq.add(txtSum);
				txtSum = "";
			}
			if (!fContinue){break;}
			if (txtLine.length() >= 1) {
				ch0 = txtLine.charAt(0);
				if (ch0 == '>'){
					fReadSeq = true;
					txtS = getWords(txtLine);
					txtProName.add(txtS[0].substring(1));
				}else{
					fReadSeq = true;
					txtSum += txtLine;
				}
			}
		}
		inData.close();

		String[][] txtRet = new String[2][txtProName.size()];
		for (int i=0; i<txtProName.size(); i++){
			txtRet[0][i] = txtProName.get(i);
			txtRet[1][i] = txtProSeq.get(i);
		}
		return txtRet;
	}

	public static void printProteinSeqs(String[][] txt){
		for (int i=0; i < txt[0].length; i++){
			System.out.println(txt[0][i]);
			System.out.println(txt[1][i]);
			//System.out.println();
		}
	}



	public static int getFileNo()
	{
		String txtToUser = "Type a number between: 1 and " + txtFilenames.length;
		for (int i=0; i < txtFilenames.length; i++)
		{
			txtToUser += "\n" + (i+1) + " : " + txtFilenames[i];
		}

		String txtFromUser = JOptionPane.showInputDialog(txtToUser); //s138

		int i = Integer.parseInt(txtFromUser) - 1;
		if (i < 0) i = -1;
		if (i >= txtFilenames.length) i = txtFilenames.length - 1;
		return i;
	}


	public static String[] getWords(String str)
	{
		str = str + " "; //not pretty!
		int lStr = str.length();
		int nWord = 0;
		boolean fFindStart = true; //str.charAt(0) == ' ';

		for(int i = 0; i < lStr; i++) {
			if (fFindStart) {
				if (str.charAt(i) != ' ') {nWord++; fFindStart = false;}}
			else {
				if (str.charAt(i) == ' ') {fFindStart = true;}}
		}

		String[] strWords = new String[nWord];
		//--
		int iWord = 0;
		int a = 01234, b = 01234; //start value don't matter
		fFindStart = true;

		for(int i = 0; i < lStr; i++) {
			if (fFindStart) {
				if (str.charAt(i) != ' ') {a = i; fFindStart = false;}}
			else {
				if (str.charAt(i) == ' ') {b = i; fFindStart = true; strWords[iWord] = str.substring(a, b); iWord++;}}
		}
		return strWords;
	}
}
